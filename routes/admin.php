<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => ['verified'],
    'as' => 'admin.',
    'namespace' => 'Admin',
], function() {
    Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function() {
        Route::group(['middleware' => ['active']], function() {
            /*======================================= Dashboard ==============================================*/
            Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
            /*==================================== END Dashboard ==============================================*/

            /*======================================= Profile ==============================================*/
            Route::get('profile', 'ProfileController@myProfile')->name('profile');
            Route::patch('update-profile/{id}', 'ProfileController@updateProfile')->name('profile.update');
            Route::post('update-password', 'ProfileController@updatePassword')->name('profile.updatePassword');
            /*==================================== END Profile ==============================================*/

            /*======================================= User ==============================================*/
            Route::post('users/avatar', 'UserController@avatar')->name('users.avatar');
            Route::post('users/changeStatus', 'UserController@changeStatus')->name('users.changeStatus');
            Route::post('users/data', 'UserController@data')->name('users.data');
            Route::resource('users', 'UserController');
            /*==================================== END User ==============================================*/

            /*======================================= Role ==============================================*/
            Route::post('roles/data', 'RoleController@data')->name('roles.data');
            Route::resource('roles', 'RoleController');
            /*==================================== END Role ==============================================*/

            /*======================================= Work Experience ==============================================*/
            Route::post('work-experiences/data', 'WorkExperienceController@data')->name('workExperience.data');
            Route::resource('work-experiences', 'WorkExperienceController');
            /*==================================== END Work Experience ==============================================*/

            /*======================================= Organization ==============================================*/
            Route::post('organization/data', 'OrganizationController@data')->name('organization.data');
            Route::resource('organization', 'OrganizationController');
            /*==================================== END Organization ==============================================*/
        });
    });
});

Route::group(['as' => 'user.', 'namespace' => 'User'], function() {
    Route::get('user/set-password/{code}', 'RegisterController@setPassword')->name('register.setPassword');
    Route::post('user/update-password/{id}', 'RegisterController@updatePassword')->name('register.updatePassword');

});
