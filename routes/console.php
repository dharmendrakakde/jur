<?php

use App\Models\Module;
use App\Models\Permission;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('clear:all', function () {
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    $this->comment('Clearing Config');
    Artisan::call('cache:clear');
    $this->comment('Clearing Cache');
    Artisan::call('route:clear');
    $this->comment('Clearing Route');
    Artisan::call('view:clear');
    $this->comment('Clearing View');
    Artisan::call('optimize');
    $this->comment('Optimize');
})->describe('Clearing Cache,View,Route,Config');

Artisan::command('add:modules', function () {
    Artisan::call('clear:all');
    foreach (config('permission.allowed_modules') as $module_slug => $module) {
        $module = Module::updateOrCreate([
            'name' => $module_slug,
        ], [
            'display_name' => json_encode($module)
        ]);
        $operations = ['read', 'add', 'edit', 'delete'];
        foreach ($operations as $operation) {
            Permission::updateOrCreate([
                'name' => $operation.'-'.$module_slug,
                'guard_name' => config('auth.defaults.guard'),
                'module_id' => $module->id
            ], []);
        }
    }
    $permissions = Permission::get()->pluck('id')->toArray();
    $adminRole = \App\Models\Role::where('name','admin')->first();
    $adminRole->syncPermissions($permissions);
    $this->comment('done');
})->describe('Translation command');