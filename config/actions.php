<?php

return [
    'edit' => [
        'icon' => 'fa-edit',
        'label' => 'admin_lang.edit_btn_label'
    ],
    'show' => [
        'icon' => 'fa-eye',
        'label' => 'admin_lang.show_btn_label'
    ],
    'delete' => [
        'icon' => 'fa-trash',
        'label' => 'admin_lang.delete_btn_label'
    ],
    'transfer' => [
        'icon' => 'fa-exchange-alt',
        'label' => 'admin_lang.transfer'
    ],
    'print' => [
        'icon' => 'fa-print',
        'label' => 'admin_lang.print_btn_label'
    ],
    'default' => [
        'icon' => 'fa fa-file'
    ]
];
