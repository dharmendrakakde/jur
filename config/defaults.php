<?php

return [
    'status' => [
        'Inactive',
        'Active',
    ],
    'roles' => [
        'admin' => [
            'name' => 'admin',
            'display_name' => [
                'en' => 'Admin',
            ],
            'permissions' => ['*'],
            'exclude_permissions' => [],
        ],
        'employee' => [
            'name' => 'employee',
            'display_name' => [
                'en' => 'Employee',
            ],
            'permissions' => [
                'read-dashboard'
            ],
        ],
    ],
    'admin' => [
        'first_name' => 'Super',
        'last_name' => 'Admin',
        'email' => 'myadmin@yopmail.com',
        'password' => '12345678',
        'status' => 'active',
        'role' => 'admin'
    ],
    'time_format' => 'j M y',
    'date_time_format' => 'j M y H:i A',
    'languages' => [
        'en' => [
            'en' => 'English',
        ],
    ]
];
