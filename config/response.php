<?php

return [
    'success' => [
        'code' => 200,
        'status' => 1,
        'title' => 'Success'
    ],
    'error' => [
        'code' => 500,
        'status' => 2,
        'title' => 'error'
    ],
    'invalid' => [
        'code' => 422,
        'status' => 2,
        'title' => 'error'
    ],

];
