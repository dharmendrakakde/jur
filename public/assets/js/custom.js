//allowed response status
const STATUS = {
    'SUCCESS' : 1,
    'ERROR': 2
};

//filter order of datatables
const DATATABLE_FILTER_ORDER = '<"filters"f<"custom-buttons"><"custom-filters">>tpil';

//datatables
var dataTable, isDropDownChanged =false, dTable;

//append csrf in every ajax
jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
    }
});

jQuery(document).ready(function() {
    //custom datatable with default options
    dataTable = function(element, url, columns, language, filters, headerHTML, indexColumn, orderDef, filterOrder, paging, footerCallback) {
        //default arguments are not supported in IE11
        //manually check if it's passed or not
        if (typeof filters == 'undefined') {
            filters = {};
        }

        if (typeof language == 'undefined') {
            language = 'en';
        }

        if (typeof headerHTML == 'undefined') {
            headerHTML = '';
        }

        if (typeof indexColumn == 'undefined') {
            indexColumn = true;
        }

        if (typeof filterOrder == 'undefined') {
            filterOrder = DATATABLE_FILTER_ORDER;
        }

        if (typeof paging == 'undefined') {
            paging = true;
        }

        if (typeof footerCallback == 'undefined') {
            footerCallback = function() {
                return;
            };
        }

        dTable = element.DataTable({
            processing: true,
            serverSide: true,
            paging: paging,
            columnDefs: [{
                "searchable": false,
                "orderable": false,
                "targets": indexColumn ? 0 : -1
            }],
            order: orderDef ? orderDef : [[0, 'asc']],
            ajax: {
                url: url,
                type: "POST",
                data: function(data) {
                    jQuery('.filter').each(function(key, element) {
                        data[jQuery(element).attr('name')] = isDropDownChanged ? jQuery(element).val() : '';
                    });
                    for (let i in filters) {
                        data[i] = isDropDownChanged ? jQuery("[name='" + i + "']").val() : getUrlParameter(i);
                    }
                    return data;
                }
            },
            columns: columns,
            language: {
                "url": ("/assets/lang/datatable-" + language + ".json")
            },
            dom: filterOrder,
            drawCallback: function(settings) {
                if (indexColumn) {
                    dTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i + 1 + dTable.page.info().page*(dTable.page.info().length);
                        dTable.cell(cell).invalidate('dom');
                    });
                }
                renderHeader(filters, dTable, headerHTML);
            },
            footerCallback: footerCallback
        });

        jQuery.getJSON("/assets/lang/daterangepicker-" + language + ".json").done(function(data) {
            let date = jQuery('.date-filter');
            let startDate = moment();
            let endDate =  moment();
            if (typeof date.attr('data-start-date') != 'undefined') {
                startDate =  moment(date.attr('data-start-date'), 'YYYY-MM-DD');
            }
            if (typeof date.attr('data-end-date') != 'undefined') {
                endDate =  moment(date.attr('data-end-date'), 'YYYY-MM-DD');
            }
            jQuery(date).daterangepicker({
                opens: 'left',
                locale: data,
                startDate: startDate,
                endDate: endDate
            }, function(start, end, label) {
                jQuery('.date-filter .form-control').val(start.format("YYYY-MM-DD") + " / " + end.format("YYYY-MM-DD"));
                jQuery('.date-filter .form-control').trigger('change');
            });
        });
        return;
    }

    //common delete functionality
    jQuery(document).on('click', '.btn-delete, .btn-confirm', function (e) {
        e.preventDefault();
        var url = jQuery(this).attr('href');
        var title = jQuery(this).attr('swal-title');
        var description  = jQuery(this).attr('swal-description');
        var confirmBtn = jQuery(this).attr('swal-confirm');
        var cancelBtn = jQuery(this).attr('swal-cancel');
        var method = jQuery(this).attr('data-method');
        if (typeof title == 'undefined') {
            title = 'Are you sure?';
        }
        if (typeof description == 'undefined') {
            description = 'Are you really sure you want to delete this entry?';
        }
        if (typeof confirmBtn == 'undefined') {
            confirmBtn = 'Yes! Delete it!';
        }
        if (typeof cancelBtn == 'undefined') {
            cancelBtn = 'Cancel';
        }
        if (typeof method == 'undefined') {
            method = 'DELETE';
        }

        Swal.fire({
            title: title,
            type: 'warning',
            html: description,
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: confirmBtn,
            cancelButtonText: cancelBtn,
            showLoaderOnConfirm: true,
          }).then(function(confirm) {
            if (confirm.value) {
                jQuery.ajax({
                    type: method,
                    data: { _method: method },
                    dataType: 'JSON',
                    url: url,
                    success: function (data) {
                        console.log(data);
                        if (data.response.status == STATUS.SUCCESS) {
                            successToast(data.message);
                            if (!data.redirect) {
                                reloadDataTable(dTable);
                            } else {
                                location.replace(data.redirect);
                            }
                        } else if (data.response.status == STATUS.ERROR) {
                            errorToast(data.message);
                        }
                    },
                });
            }
        });
    });

    //common form submit
    jQuery(document).on('click', '.ajax-submit', function (e) {
		e.preventDefault();
		e.stopPropagation();
		var submit_btn = jQuery(this);
		var btn_name = jQuery(this).html();
		var new_btn_name = 'Loading...'
		var form = jQuery(this).parents('form:first');
		var method = jQuery(form).attr('method');
		var url = jQuery(form).attr('action');

		formSubmit(url, method, submit_btn, btn_name, new_btn_name, form);
    });

    jQuery(document).on('click', '.ajax-submit-diff', function (e) {
		e.preventDefault();
		e.stopPropagation();
		var submit_btn = jQuery(this);
		var btn_name = jQuery(this).html();
		var new_btn_name = 'Loading...'
		var form = jQuery(this).parents('form:first');
		var method = jQuery(form).attr('method');
		var url = jQuery(this).attr('href');

		formSubmit(url, method, submit_btn, btn_name, new_btn_name, form);
    });

    //common button input 
    jQuery(document).on('click', '.btn-input', function(e) {
        e.preventDefault();
        var url = jQuery(this).attr('href');
        var title = jQuery(this).attr('swal-title');
        var description  = jQuery(this).attr('swal-description');
        var confirmBtn = jQuery(this).attr('swal-confirm');
        var cancelBtn = jQuery(this).attr('swal-cancel');
        if (typeof title == 'undefined') {
            title = 'Give your Input!';
        }
        if (typeof description == 'undefined') {
            description = 'Please Enter Something!';
        }
        if (typeof confirmBtn == 'undefined') {
            confirmBtn = 'Yes! Save it!';
        }
        if (typeof cancelBtn == 'undefined') {
            cancelBtn = 'Cancel';
        }
        Swal.fire({
            title: title,
            type: 'info',
            html: description,
            input: 'textarea',
            focusConfirm: false,
            confirmButtonText: confirmBtn,
            cancelButtonText: cancelBtn,
            showLoaderOnConfirm: true,
          }).then(function(confirm) {
            if (confirm.value) {
                jQuery.ajax({
                    type: 'POST',
                    data: { note: confirm.value },
                    dataType: 'JSON',
                    url: url,
                    success: function (data) {
                        if (data.response.status == STATUS.SUCCESS) {
                            successToast(data.message);
                            if (!data.redirect) {
                                reloadDataTable(dTable);
                            } else {
                                location.replace(data.redirect);
                            }
                        } else if (data.response.status == STATUS.ERROR) {
                            errorToast(data.message);
                        }
                    },
                });
            }
        });
    });
});

//get params from url jquery
var getUrlParameter = function(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}

//draw datatable filters
var renderHeader = function(filters, dTable, headerHTML) {
    if (jQuery('.filters .custom-filters').children().length <= 0) {
        //append filters beside search input
        let html = '';
        for (let i in filters) {
            html += '<select name="' + i + '" class="form-control form-control-sm filter">';
            for (let j in filters[i]) {
                html += '<option value="' + filters[i][j] + '">' + j.charAt(0).toUpperCase() + j.slice(1) + '</option>';
            }
            html += '</select>';
        }
        jQuery('.filters .custom-filters').append(html + '<a href="javascript:void(0)" class="reset-filter"><i class="fa fa-times"></i></a>');
        
        //if any filter changes then reload datatable based on all combine filters
        jQuery(document).on('change', '.filter', function() {
            isDropDownChanged = true;
            reloadDataTable(dTable);
            jQuery('.reset-filter').css('display', 'none');
            jQuery('.filter').each(function(key, element) {
                if (jQuery(element).val() != '') {
                    jQuery('.reset-filter').css('display', 'block');
                } 
            });
        });

        //select dropdowns if url has param
        for (let i in filters) {
            let param = getUrlParameter(i);
            if (typeof param != 'undefined' && param != '') {
                jQuery("[name='" + i + "']").val(param);
                jQuery('.reset-filter').css('display', 'block');
            }
        }

        jQuery(document).on('click', '.reset-filter', function() {
            jQuery('.filter').val('');
            isDropDownChanged = true;
            reloadDataTable(dTable.search('').draw());
            jQuery(this).css('display', 'none');
        });

        //append buttons html
        jQuery('.custom-buttons').html(headerHTML);
    }
}

//reload datatables
var reloadDataTable = function(table) {
    table.ajax.reload();
}

$("#varient_form_btn").click(function(){
    $("#add_new_varient").show();
    $(".varien_block").show();
});

jQuery(document).ready(function($) {

    $(".input_text").focus(function(){
        $(this).parent().addClass("input_text_wrapper--focus");
   
    }).blur(function(){
        $(this).parent().removeClass("input_text_wrapper--focus");
    })
   
});    



// $("input[name='quantity']").TouchSpin({
//     min: 0,
//     max: 100
// });


$(window).scroll(function(){
    var s = $(window).scrollTop();
    var h = $('header');
    var e_h = $('header').outerHeight();
    if (s > 200) {
        $('header').css('margin-bottom', e_h);
        $('header').addClass('fixed_header');
        $('.scroll_top').addClass('scroll_top-active');
    }
    else if(s < 200){
        $('header').css('margin-bottom', 0);
        $('header').removeClass('fixed_header');
        $('.scroll_top').removeClass('scroll_top-active');
    }
});

// $('.scroll_top').on("click",function(){
//     $(window).scrollTop(0);
// });

//Smooth Scrolling ---------
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
  // ----------------


jQuery(document).ready(function($) {

    $(".input_text").focus(function () {
        $(this).parent().addClass("input_text_wrapper--focus");

    }).blur(function () {
        $(this).parent().removeClass("input_text_wrapper--focus");
    })
});

function formSubmit(url, method, submit_btn, btn_name, new_btn_name, form) {
    jQuery(form).ajaxSubmit({
        type: method,
        url: url,
        enctype: "multipart/form-data",
        dataType: 'JSON',
        beforeSend: function () {
            jQuery(submit_btn).html(new_btn_name);
            jQuery(submit_btn).attr('disabled', true);
            jQuery(form).find('.form-group').removeClass('has-error');
            jQuery(form).find('.help-block').remove();
            showLoading();
        },
        success: function (data) {
            if (data.response.status == STATUS.SUCCESS) {
                if (data.redirect) {
                    location.replace(data.redirect);
                } else {
                    location.reload();
                }
            } else {
                if (data.redirect) {
                    location.replace(data.redirect);
                }
                errorToast(data.message);
            }
        },
        error: function (data) {
            jQuery.each(data.responseJSON.errors, function (key, index) {
                if (~key.indexOf(".")) {
                    key = key.replace(/\./gi, '-');
                    jQuery('#' + key).closest('.form-group').addClass('has-error').append('<span class="help-block" title="' + index[0] + '">' + index[0] + '</span>');
                } else {
                    var input = jQuery(form).find('[name="' + key + '"]');
                    if (input.length < 1 ) {
                        // key = key + '[]';
                        input = jQuery(form).find('[name="' + key + '[]"]');
                        // console.log(input)
                    }
                    input.closest('.form-group').addClass('has-error').append('<span class="help-block" title="' + index[0] + '">' + index[0] + '</span>');
                }
            });
        },
        complete: function () {
            jQuery(submit_btn).html(btn_name);
            jQuery(submit_btn).attr('disabled', false);
            hideLoading();
        }
    });
}

function digitAndPoint(evt, id) {
    try {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) {
            var txt = document.getElementById(id).value;
            if (!(txt.indexOf(".") > -1)) {

                return true;
            }
        }
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    } catch (w) {
        // alert(w);
    }
}

// $("input[name='quantity']").TouchSpin({
//     min: 0,
//     max: 100
// });
