const STATUS = {
    'SUCCESS' : 1,
    'ERROR': 2
};

jQuery(document).ready(function(){
    //append csrf in every ajax
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".h_login_btn,.login-popup").click(function(e){
        e.preventDefault();
        $(".pop_model#signup_pop_box .close_popup").click();
        $(".pop_model#login_pop_box").addClass("login_active");
    });
    // $(".login_popup_close").click(function(){
    //     $(".pop_model").removeClass("login_active");
    // });

    $(".h_sign_up_btn").click(function(e){
        e.preventDefault();
        $(".pop_model#login_pop_box .close_popup").click();
        $(".pop_model#signup_pop_box").addClass("login_active");
    });
    // $(".login_popup_close").click(function(){
    //     $(".pop_model").removeClass("login_active");
    // });
});

jQuery(document).on('click', '.form-submit', function(e){
    e.preventDefault();
    e.stopPropagation();

    var submit_btn = jQuery(this);
    var btn_name = jQuery(this).html();
    var new_btn_name = 'Loading...'
    var form = jQuery(this).parents('form:first');
    var method = jQuery(form).attr('method');
    var url = jQuery(form).attr('action');

    // jQuery(form).ajaxSubmit({
    jQuery.ajax({
        type: method,
        url: url,
        enctype: "multipart/form-data",
        dataType: 'JSON',
        // data: jQuery(form).serialize(),
        data: new FormData(form[0]),
        processData: false,
        contentType: false,
        beforeSend: function () {
            jQuery(submit_btn).html(new_btn_name);
            jQuery(submit_btn).attr('disabled', true);
            jQuery(form).find('.input_text_wrapper').removeClass('has-error');
            jQuery(form).find('.help-block').remove();
            showLoading();
        },
        success: function(data){
            hideLoading();
            if (data.response.status == STATUS.SUCCESS) {
                if ( data.message ){
                    successToast(data.message);
                }
                if (data.redirect) {
                    location.replace(data.redirect);
                } else {
                    location.reload();
                }
            } else {
                if (data.redirect) {
                    location.replace(data.redirect);
                }
                errorToast(data.message);
            }
        },
        error: function(data){
            jQuery.each(data.responseJSON.errors, function (key, index) {
                if (~key.indexOf(".")) {
                    key = key.replace(/\./gi, '-');
                    // jQuery('#' + key).closest('.input_text_wrapper').addClass('has-error').append('<span class="help-block">' + index[0] + '</span>');
                    jQuery('#' + key).closest('.form_group').addClass('has-error').append('<span class="help-block">' + index[0] + '</span>');
                } else {
                    var input = jQuery(form).find('[name="' + key + '"]');
                    // input.closest('.input_text_wrapper').addClass('has-error').append('<span class="help-block">' + index[0] + '</span>');
                    input.closest('.form_group').addClass('has-error').append('<span class="help-block">' + index[0] + '</span>');
                }
            });
            hideLoading();
        },
        complete: function () {
            jQuery(submit_btn).html(btn_name);
            jQuery(submit_btn).attr('disabled', false);
            hideLoading();
        }
    });
});

$(".quantity").TouchSpin({
    min: 1,
    max: 100
});
$("input[name='qty']").TouchSpin({
    min: 0,
    max: 100
});

$(".cart_btn").click(function(){
    $(".cartCount").addClass("cart_add");
});

$('.wish_btn').click(function(){
    $(this).toggleClass('active');
});

$(".cart_link").click(function(){
    $(".cart_popup").addClass("cart_popup-active");
});
$(".close_popup").click(function(){
    $(".cart_popup").removeClass("cart_popup-active");
});
$(".continue_btn").click(function(){
    $(".cart_popup").removeClass("cart_popup-active");
});

$(".cart_btn").click(function(){
    $(".cartCount").addClass("cart_add");
});


$('body').on("click", ".cart_link", function(){
    $(".cart_popup").addClass("cart_popup-active");
    $('.popUp_overlay').css('display', 'block');
});
$('body').on("click", ".close_popup", function(){
    $(".cart_popup").removeClass("cart_popup-active");
    $('.popUp_overlay').css('display', 'none');
});
$('body').on("click", ".continue_btn", function(){
    $(".cart_popup").removeClass("cart_popup-active");
    $('.popUp_overlay').css('display', 'none');
});
$( ".popUp_overlay" ).click(function() {
    $(this).css('display', 'none');
    $(".cart_popup").removeClass("cart_popup-active");
    $(".pop_model").removeClass("popup-active");

});

$('body').on("click", ".h_login_btn,.login-popup", function(e){
    e.preventDefault();
    $(".pop_model#login_pop_box").addClass("popup-active");
    $('.popUp_overlay').css('display', 'block');
});
$('body').on("click", ".h_sign_up_btn", function(){
    $(".pop_model#signup_pop_box").addClass("popup-active");
    $('.popUp_overlay').css('display', 'block');
});
$('body').on("click", ".close_popup", function(){
    $(".pop_model").removeClass("popup-active");
    $('.popUp_overlay').css('display', 'none');
});

$( ".flaticon-search" ).click(function() {
    $(".form_search").addClass("search_popup-active");

});


// $( ".categories_content" ).each(function(index) {
//     var a = $(this);
//     a.find(".more_category_btn").click(function(){
//         a.find(".categories_content__check_boxes").toggleClass("categories_check_boxes_open");
//         var t = $(this);
//         t.toggleClass('more_category_btn');
//         if(t.hasClass('more_category_btn')){
//             t.text('See more');         
//         } else {
//             t.text('See less');
//         }
//     });
// })


$('body').on("click", ".region_pannel", function(){
    $(".sub_pannel").addClass("pannel_popup-active");
    $('.popUp_overlay').css('display', 'block');
});
$( ".popUp_overlay" ).click(function() {
    $(this).css('display', 'none');
    $(".sub_pannel").removeClass("pannel_popup-active");

});

// Country Select 
$(function(){
    $('.selectpicker').selectpicker();
});


$(".add_addres_btn").click(function(e){
    e.preventDefault();
    $("#add_new_address_block").removeClass("hidden");
    $("#add_new_address_block .add_address_form").removeClass("hidden");
})

$(".mob_filter_heading button").click(function(e) {
    $(".filter_mobile_visible").addClass("filter_mobile_open");
})
$(".sec_title .la-remove").click(function(e) {
    $(".filter_mobile_visible").removeClass("filter_mobile_open");
})

jQuery(document).ready(function(){
    var width = $(window).width();
    if (width <= 767) {
        $(".filter_col").addClass("filter_mobile_visible");
        $(".mid_header_bar .fix_nav_categories").addClass("mobile_nav");
        $(".fix_menu_icon__icon").click(function(e){
            $(".mobile_nav").addClass("mobile_nav_open");
            $(".popUp_overlay").css("display","block");
            $("html").css("overflow", "hidden");
        })
        // $(".icon_close_nav").click(function(e){
            
        // })
        $(".category_nav__block--item button").click(function(e) { 
            // $($(this).find($(".sb_menu_block"))[0]).css("left", 0);
            $(this).parent().find(".sb_menu_block").css("left", 0);
        });
        $(".icon_close_nav").click(function(e){
            $(".mobile_nav").removeClass("mobile_nav_open");
            $(".popUp_overlay").css("display","none");
            // $(".sb_menu_block").css("left", -100);
            $("html").css("overflow", "auto");
        });
        $(".icon_back_nav").click(function(e){
            // $($(this).find($(".sb_menu_block"))[0]).css("left", 0);
            $($(this).closest(".sb_menu_block")[0]).css("left", "-100%");
            $(".sb_menu_block").each(function() {
                // $( this ).addClass("sub_drwer_back");
            });
        });
    }
    else {
        $(".filter_col").removeClass("filter_mobile_visible");
        $(".mid_header_bar .fix_nav_categories").removeClass("mobile_nav");
    }
})

$(".view_change_icon").click(function(e){
    $(".pd_content .product_section .product_item").toggleClass("layout_one");
    $(this).toggleClass("list_one");
})

$(".icon_back_nav").click(function(e){
    
});

$(".mobile_search_icon").click(function(e){
    $(".hd_search_box").addClass("mobile_search_open");
    $(".righr_mid_block").hide();
    $(".hd_logo").hide();
})
$(".mb_cancel").click(function(e){
    $(".hd_search_box").removeClass("mobile_search_open");
    $(".righr_mid_block").show();
    $(".hd_logo").show();
})

$(".add_comment").click(function(e){
    $(".your_comment_box").removeClass("hide");
});

$(".comment_cancel").click(function(e){
    $(".your_comment_box").addClass("hide");
});
