# README #
Jur. 

Laravel Framework version 8.50

### System Requirements ###
* PHP >= 7.3
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### How do I get set up? ###

* clone the project
* composer install
* set-up .env
* add mail credentials
    * MAIL_DRIVER=
    * MAIL_HOST=
    * MAIL_PORT=
    * MAIL_USERNAME=
    * MAIL_PASSWORD=
    * MAIL_ENCRYPTION=
    * MAIL_FROM_NAME="${APP_NAME}"
    * MAIL_FROM_ADDRESS="no-reply@jur.io"
* php artisan key:generate
* php artisan migrate --seed
* php artisan optimize:clear
* php artisan serve

### Who do I talk to? ###

* Dharmendra Kakde - dharmendrakakde@gmail.com
