<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class UserCreateMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user,$setPwdUrl;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->user = $data['user'];
        $this->setPwdUrl = $data['setPwdUrl'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setPwdUrl = $this->setPwdUrl;
        Log::info('INSIDE MAIL => '.$setPwdUrl);
        return $this->from('no-replay@system.com', 'system admin')
            ->subject('Registration successfully complete')
            ->view('emails.user_create',[
                'user' => $this->user,
                'setPwdUrl' => $setPwdUrl
            ]);
    }
}