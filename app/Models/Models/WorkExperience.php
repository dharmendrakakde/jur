<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    use HasFactory;

    CONST WORD_COUNT = 300, WORKING_IN_SAME = 1;

    protected $guarded = ['id'];

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
    ];

}
