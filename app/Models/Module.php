<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class Module extends Model
{
    protected $guarded = ['id'];

    public $translatable = ['display_name'];

    protected $casts = [
        'display_name' => 'array'
    ];

    /**
     * Get list of all permisssion that this module has
     */
    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
