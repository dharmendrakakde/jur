<?php

namespace App\Models;

use Spatie\Permission\Models\Role as BaseRole;
use Spatie\Translatable\HasTranslations;

class Role extends BaseRole
{
    use HasTranslations;

    public static $status = [
        'active' => [
            'code' => 1,
            'slug' => 'active'
        ],
        'inactive' => [
            'code' => 0,
            'slug' => 'inactive'
        ]
    ];

    public static $filter_keys = [
        'status' => 'status',
        'module' => 'module',
//        'is_vendor' => 'is_vendor'
    ];

    public $translatable = ['display_name'];

    protected $casts = [
        'display_name' => 'array'
    ];

    /**
     * Get only active roles
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::$status['active']['code']);
    }
}
