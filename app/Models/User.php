<?php

namespace App\Models;

use App\Models\Models\WorkExperience;
use App\Notifications\PasswordReset;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable, HasRoles;
    const PASSWORD_NOT_SET = 0, PASSWORD_SET = 1;
    const IN_ACTIVE = 0, ACTIVE = 1, PENDING = 2;

    public static $status = [
        'active' => [
            'code' => 1,
            'slug' => 'active'
        ],
        'inactive' => [
            'code' => 0,
            'slug' => 'inactive'
        ],
        'pending' => [
            'code' => 2,
            'slug' => 'pending'
        ]
    ];

    public static $filter_keys = [
        'role' => 'role',
        'status' => 'status'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'avatar',
        'first_name',
        'last_name',
        'email',
        'password',
        'last_login',
        'role_id',
        'set_password',
        'verification_code',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $guarded = [
        'id',
    ];

    protected $dates = ['deleted_at', 'last_login'];

    public function scopePasswordNotSet($query)
    {
        return $query->where('users.set_password', self::PASSWORD_NOT_SET);
    }

    public function scopeActive($query)
    {
        return $query->where('users.status', self::$status['active']['code']);
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function workExperience()
    {
        return $this->hasMany(WorkExperience::class, 'user_id','id');
    }
}
