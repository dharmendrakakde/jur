<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    /**
     * Get module to which these permission belongst To.
     */
    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
