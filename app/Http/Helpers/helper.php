<?php

use App\Models\Role;


/**
 * Translate string to current locale
 *
 * @param array $json_str
 * @param string $lang
 * @return string
 */
function translate($json_str, $lang = false)
{
//    dump($json_str);
    return (is_string($json_str) ? json_decode($json_str, true) : $json_str)[$lang ? $lang : app()->getLocale()] ?? $json_str;
}

if(!function_exists('hasPermissionWithActiveRole')){
    function hasPermissionWithActiveRole($permission, $user)
    {
        $roles = Role::active()->whereHas('users', function ($users) use ($user) {
            $users->where('id', $user->id);
        })->get();
        foreach ($roles as $role) {
            if ($role->hasPermissionTo($permission)) {
                return true;
            }
        }
        return false;
    }
}

/**
 * Generate random string of specifi length
 *
 * @param int $length
 * @return string
 */
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
