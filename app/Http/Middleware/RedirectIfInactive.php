<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class RedirectIfInactive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->status == User::$status['inactive']['code']) {
            auth()->logout();
            return redirect()->route('login')->with(['error' => 'Account is Inactive!']);
        }
        if (auth()->check() && auth()->user()->status == User::$status['pending']['code'] && auth()->user()->email_verified_at == null) {
            return redirect()->route('verification.verify');
        }
        return $next($request);
    }
}
