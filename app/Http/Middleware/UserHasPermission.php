<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Exceptions\UnauthorizedException;

class UserHasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = '')
    {
        if (auth()->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        //permissions will be specified through route property 'can'
        //if it's not specified then route will be accessible by default
        $actions = Route::getCurrentRoute()->action['can'] ?? '';

        //if any permission is required for accessing route
        if (!empty($actions)) {
            if (strpos($actions, '|')) {
                //user will need only one permission from list specified
                $permissions = explode('|', $actions);
                if (!auth()->user()->hasAnyPermission($permissions)) {
                    throw UnauthorizedException::forRolesOrPermissions($permissions);
                }
            } else {
                //user will need all permissions specified
                $permissions = explode(',', $actions);
                if (!auth()->user()->hasAllPermissions($permissions)) {
                    throw UnauthorizedException::forRolesOrPermissions($permissions);
                }
            }
        } else {
            if (hasPermissionWithActiveRole($permission, auth()->user())) {
                return $next($request);
            }
        }
        throw UnauthorizedException::forRolesOrPermissions([$permission]);
    }
}
