<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function setPassword($code){
        if(!$user = User::passwordNotSet()->whereId(decrypt($code))->first()){
            return redirect(route('login'))->with('error',trans('admin_lang.invalid_code'));
        }
        return view('admin.users.set_password',[
            'user' => $user
        ]);
    }
    public function updatePassword(Request $request,$id){
        $this->__validate($request);
        try {
            if(!$user = User::passwordNotSet()->whereId($id)->first()){
                return response()->json([
                    'response' => config('response.error'),
                    'message' => trans('admin_lang.invalid_code'),
                    'redirect' => route('login')
                ]);
            }
            DB::beginTransaction();
            $user->password = Hash::make($request->password);
            $user->status = User::$status['active']['code'];
            $user->verification_code = null;
            $user->set_password = User::PASSWORD_SET;
            $user->email_verified_at = Carbon::now();
            $user->save();

            DB::commit();
            session()->flash('success', trans('admin_lang.password_set_successfully'));
            return response()->json([
                'response' => config('response.success'),
                'redirect' => route('login')
            ]);
        } catch (\Exception $e) {
            if(config('app.app_type') == 'local'){
                return response()->json([
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ]);
            }
            return response()->json([
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ]);
        }
//        return redirect(route('admin.login.index'))->with('success',trans('admin_lang.password_set_successfully'));
    }
    private function __validate(Request $request, $is_update = false)
    {

        $rules = [
            'password' => 'required|confirmed|min:8',
//            'password' => 'required|min:8',
        ];
        return $this->validate($request, $rules);
    }
}
