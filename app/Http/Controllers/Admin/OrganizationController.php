<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Models\Organization;
use App\Rules\OrganizationWordCount;
use App\Traits\WorkExperienceDateCheck;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class OrganizationController extends Controller
{
    use WorkExperienceDateCheck;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.organization.index');
    }

    //Ajax loading of data for datatables
    public function data(Request $request)
    {
        $organization = Organization::where('user_id', Auth::id());

        return DataTables::of($organization)
            ->editColumn('name', function ($organization) {
                return $organization->name;
            })
            ->editColumn('title', function ($organization) {
                return $organization->title;
            })
            ->editColumn('start_date', function ($organization) {
                return $organization->start_date->translatedFormat(config('defaults.time_format'));
            })
            ->editColumn('end_date', function ($organization) {
                return $organization->end_date ? $organization->end_date->translatedFormat(config('defaults.time_format')) : 'Present';
            })
            ->addColumn('created', function ($organization) {
                return $organization->created_at;
            })
            ->addColumn('action', function ($organization) {
                return view('admin.organization.partials.actions', compact('organization'));
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->__validate($request);
        try {
            $checkDateValidate = $this->dateValidate($request);

            if ($checkDateValidate['status'] == false) {
                session()->flash('error', $checkDateValidate['message']);
                return response()->json([
                    'status' => false,
                    'response' => config('response.error'),
                    'message' => $checkDateValidate['message'],
                ]);
            }

            DB::beginTransaction();
            $request['user_id'] = Auth::id();

            $request['start_date'] = Carbon::parse($request->start_date)->format('Y-m-d');
            $request['end_date'] = $request->end_date == null ? null : Carbon::parse($request->end_date)->format('Y-m-d');

            Organization::create($request->only(
                'user_id', 'name', 'title', 'start_date', 'end_date', 'description','is_same_role'
            ));

            DB::commit();
            session()->flash('success', trans('admin_lang.work_experience_created'));
            return [
                'response' => config('response.success'),
                'redirect' => route('admin.organization.index')
            ];
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage(),
                    'error' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something'),
            ];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organization = Organization::findOrFail($id);

        return view('admin.organization.edit', with([
            'organization' => $organization,
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organization = Organization::findOrFail($id);

        $this->__validate($request, $organization->id);

        try {
            $checkDateValidate = $this->dateValidate($request);

            if ($checkDateValidate['status'] == false) {
                session()->flash('error', $checkDateValidate['message']);
                return response()->json([
                    'status' => false,
                    'response' => config('response.error'),
                    'message' => $checkDateValidate['message'],
                ]);
            }

            $request['start_date'] = Carbon::parse($request->start_date)->format('Y-m-d');
            $request['end_date'] = Carbon::parse($request->end_date)->format('Y-m-d');

            $organization->update([
                'name' => $request->name,
                'title' => $request->title,
                'start_date' => $request->start_date,
                'end_date' => $request->is_same_role == Organization::WORKING_IN_SAME ? null : $request->end_date,
                'description' => $request->description,
                'is_same_role' => $request->is_same_role,
            ]);

            session()->flash('success', trans('admin_lang.work_experience_updated'));
            return [
                'response' => config('response.success'),
                'redirect' => route('admin.organization.index')
            ];
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $organization = Organization::findOrFail($id);
            $organization->delete();
            return [
                'response' => config('response.success'),
                'title' => trans('admin_lang.success'),
                'message' => trans('admin_lang.user_deleted'),
                'reload' => true
            ];
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'title' => trans('admin_lang.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'title' => trans('admin_lang.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    /**
     * @param Request $request
     * @param bool $is_update
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    private function __validate(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'title' => 'required|max:255',
            'start_date' => 'required',
            'end_date' => 'required_if:is_same_role,==,0',
            'description' => new OrganizationWordCount(),
        ];

        $msg = [
            'end_date.required_if' => 'End date is required'
        ];

        return $this->validate($request, $rules, $msg);
    }
}
