<?php

namespace App\Http\Controllers\Admin;

use App\Models\Module;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;

class RoleController extends Controller
{
    public function index()
    {
        $modules = Module::all()->mapWithKeys(function($value, $key) {
            return [translate($value->display_name) => $value->name];
        })->prepend('', trans('admin_lang.select_module'));

        $role_status = [trans('admin_lang.select_status') => ''];
        foreach (Role::$status as $key => $status) {
            $role_status[trans('admin_lang.status_'.$status['slug'])] = $status['code'];
        }

        $filters = [
            Role::$filter_keys['status'] => $role_status,
            Role::$filter_keys['module'] => $modules,
        ];
        return view('admin.roles.index', compact('filters'));
    }

    //ajax loading of data for datatables
    public function data(Request $request)
    {
        $roles = Role::with('permissions.module');
        //apply filters if available
        if ($request->has(Role::$filter_keys['status']) && $request[Role::$filter_keys['status']] != '') {
            $roles = $roles->where('status', $request[Role::$filter_keys['status']]);
        }

        if ($request->has(Role::$filter_keys['module']) && !empty($request[Role::$filter_keys['module']])) {
            $roles = $roles->whereHas('permissions.module', function($modules) use ($request) {
                $modules->where('name', strip_tags($request[Role::$filter_keys['module']]));
            });
        }
        $roles = $roles->withCount(['users'])->get();

        return DataTables::of($roles)
            ->rawColumns(['users', 'status'])
            ->addColumn('users', function($roles) {
                return '<a href="'.route('admin.users.index', [User::$filter_keys['role'] => $roles->name]).'">'.$roles->users_count.'</a>';
            })
            ->editColumn('display_name', function($roles) {
                return $roles->display_name;
            })
            ->addColumn('modules', function($roles) {
                $modules = Module::with('permissions');
                if (!$roles->permissions->pluck('name')->contains('*')) {
                    $modules = $modules->whereIn('id', $roles->permissions->groupBy('module_id')->keys());
                }
                $modules = $modules->get();
                return view('admin.roles.partials.modules', compact('roles', 'modules'));
            })
            ->editColumn('status', function($roles) {
                return view('admin.roles.partials.status', compact('roles'));
            })
            ->addColumn('action', function($roles) {
                return view('admin.roles.partials.actions', compact('roles'));
            })
            ->removeColumn(['name', 'guard_name', 'permissions', 'created_at', 'updated_at'])
            ->make(true);
    }

    public function create()
    {
        $permissions = Permission::select('id','name','module_id')->get()->groupBy('module.name');
        $modules = array_keys($permissions->toArray());
        return view('admin.roles.create', compact('modules','permissions'));
    }

    public function store(Request $request)
    {
        $this->__validate($request);

        try {
            $role = Role::create([
                'name' => $request->name,
                'display_name' => $request->display_name,
                'guard_name' => config('auth.defaults.guard'),
                'status' => $request->status ?? Role::$status['inactive']['code']
            ]);

            $permissions = $request->permissions;
            $role->givePermissionTo($permissions);
            session()->flash('success', trans('admin_lang.role_created'));
            return [
                'response' => config('response.success'),
                'redirect' => route('admin.roles.index')
            ];
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    public function edit($role_id)
    {
        $role = Role::with('permissions')
            //->whereNotIn('name', array_merge(['admin'], auth()->user()->roles->pluck('name')->toArray()))
            ->findOrFail($role_id);

        $existingPermissions = $role->permissions->pluck('id')->toArray();
        $permissions = Permission::select('id','name','module_id')->get()->groupBy('module.name');
        $modules = array_keys($permissions->toArray());
        return view('admin.roles.edit', compact('role', 'modules','permissions','existingPermissions'));
    }

    public function update(Request $request, $role_id)
    {
        $role = Role::with('permissions')
            //->whereNotIn('name', array_merge(['admin'], auth()->user()->roles->pluck('name')->toArray()))
            ->findOrFail($role_id);
        $this->__validate($request, $role->id);

        try {
            $role->update([
                'display_name' => $request->display_name,
                'status' => $request->status ?? Role::$status['inactive']['code']
            ]);

            $permissions = $request->permissions;
            $role->syncPermissions($permissions);

            session()->flash('success', trans('admin_lang.role_updated'));
            return [
                'response' => config('response.success'),
                'redirect' => route('admin.roles.index')
            ];
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    /**
     * @param Request $request
     * @param bool $is_update
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    private function __validate(Request $request, $is_update = false)
    {
        $request->merge(['name' => Str::slug($request->name)]);

        $unique_rule = 'unique:roles,name';
        if ($is_update) {
            $request->merge(['name' => '-']);
            $unique_rule .= ','.$is_update;
        }

        return $this->validate($request, [
            'name' => 'required|max:255|alpha_dash|'.$unique_rule,
            'display_name.*' => 'required|max:255',
            'status' => 'boolean',
        ]);
    }
}
