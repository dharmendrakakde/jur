<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\UserCreateMail;
use App\Models\Category;
use App\Models\Country;
use App\Models\Department;
use App\Models\User;
use App\Models\UserAuthentication;
use App\Models\UserIdentity;
use App\Models\UserVehicle;
use App\Notifications\UserCreated;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        if (!auth()->user()->can('read-users')) {
            abort(401);
        }
        $roles = Role::active();
        $roles = $roles->get()->mapWithKeys(function ($value, $key) {
            return [$value->display_name => $value->name];
        })->prepend('', trans('admin_lang.select_role'));

        $user_status = [trans('admin_lang.select_status') => ''];
        foreach (User::$status as $key => $status) {
            $user_status[trans('admin_lang.status_' . $status['slug'])] = $key;
        }

        $filters = [
            User::$filter_keys['role'] => $roles,
            User::$filter_keys['status'] => $user_status
        ];
        return view('admin.users.index', compact('filters'));
    }

    //Ajax loading of data for datatables
    public function data(Request $request)
    {
        if (!auth()->user()->can('read-users')) {
            abort(401);
        }
        $users = User::with('roles');

        //apply filters if they are selected
        if ($request->has(User::$filter_keys['status']) && $request[User::$filter_keys['status']] != '') {
            $users = $users->where('status', (User::$status[strip_tags($request[User::$filter_keys['status']])]['code']) ?? '');
        }

        if ($request->has(User::$filter_keys['role']) && !empty($request[User::$filter_keys['role']])) {
            $users = $users->whereHas('roles', function ($roles) use ($request) {
                $roles->active()->where('name', strip_tags($request[User::$filter_keys['role']]));
            });
        }

        //$users = $users->where('id','!=',auth()->id());
        $users = $users->get();

        $status = [];
        //dynamic status badges from users model
        foreach (User::$status as $key => $value) {
            $status[$value['code']] = $value;
            $status[$value['code']]['key'] = $key;
        }

        return DataTables::of($users)
            ->rawColumns(['email', 'status', 'avatar'])
            ->editColumn('avatar', function ($users) {
                $html = '<div>';
                if ($users->avatar != null) {
                    $html .= '<img src="' . asset($users->avatar) . '" alt="photo" style="border-radius: 10%; width:100px">';
                } else {
                    $html .= '<span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success" style="background: rgb(0 62 249 / 16%);">' . $users->name[0] . '</span>';
                }

                $html .= '</div>';
                return $html;
            })
            ->editColumn('name', function ($users) {
                return $users->name;
            })
            ->addColumn('roles', function ($users) {
                return view('admin.users.partials.roles', with(['roles' => $users->roles]));
            })
            ->editColumn('email', function ($users) {
                return '<a href="mailto:' . $users->email . '">' . $users->email . '</a>';
            })
            ->editColumn('status', function ($users) use ($status) {
                $dropdown = 'getDropdown';
                if (auth()->id() == $users->id || $users->hasRole('admin')) {
                    $dropdown = '';
                }
                $html = '<div class="' . $dropdown . '" data-id="' . $users->id . '" style="cursor: pointer;">';
                $html .= '<a href="javascript:void(0);" data-id="' . $users->id . '"><span class="kt-badge kt-badge--inline status-' . $status[$users->status]['slug'] . '">' . trans('admin_lang.status_' . $status[$users->status]['slug']) . '</span></a>';
                $html .= '</div>';
                return $html;
            })
            ->addColumn('created', function ($users) {
                return $users->created_at->translatedFormat(config('defaults.time_format'));
            })
            ->addColumn('last_login_time', function ($users) {
                return !empty($users->last_login) ? $users->last_login->diffForHumans() : trans('admin_lang.not_yet');
            })
            ->addColumn('action', function ($users) {
                return view('admin.users.partials.actions', compact('users'));
            })
            ->make(true);
    }

    public function create()
    {
        if (!auth()->user()->can('add-users')) {
            abort(401);
        }
        $roles = Role::active()->pluck('display_name', 'id');
        return view('admin.users.create', with([
            'roles' => $roles,
        ]));
    }

    public function store(Request $request)
    {
        if (!auth()->user()->can('add-users')) {
            abort(401);
        }
        $this->__validate($request);
        try {
            $request->merge([
                'password' => Hash::make(generateRandomString(10)),
                'status' => User::$status['active']['code'],
                'created_by' => auth()->id()
            ]);
            DB::beginTransaction();
            $user = User::create($request->only(
                'first_name', 'last_name', 'email', 'password'
            ));

            $user->set_password = User::PASSWORD_NOT_SET;
            $user->status = User::$status['pending']['code'];
            $user->verification_code = generateRandomString(10);
            $user->save();

            $user->roles()->attach($request->role);

            $setPwdUrl = route('user.register.setPassword', ['code' => encrypt($user->id)]);
            $data = [
                'user' => $user,
                'setPwdUrl' => $setPwdUrl
            ];
            //Mail::to($user->email)->queue(new UserCreateMail($data));
            Notification::send($user, new UserCreated($data));

            DB::commit();
            session()->flash('success', trans('admin_lang.user_created'));
            return [
                'response' => config('response.success'),
                'redirect' => route('admin.users.index')
            ];
        } catch (\Exception $e) {
            DB::rollback();
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something'),
                'error' => $e->getMessage()
            ];
        }
    }

    public function show($user_id)
    {
        if (!auth()->user()->can('read-users')) {
            abort(401);
        }
        $user = User::with('roles')->findOrFail($user_id);
        $roles = Role::active()->get();
        return view('admin.users.show', compact('user', 'roles'));
    }

    public function edit($user_id)
    {
        if (!auth()->user()->can('edit-users')) {
            abort(401);
        }
        $user = User::with('roles')->findOrFail($user_id);

        $roles = Role::active()->get();

        return view('admin.users.edit', with([
            'user' => $user,
            'roles' => $roles,
        ]));
    }

    public function update(Request $request, $user_id)
    {
        if (!auth()->user()->can('edit-users')) {
            abort(401);
        }
        $user = User::with('roles')->findOrFail($user_id);
        $this->__validate($request, $user->id);
        try {

            $user->update($request->only(
                'first_name', 'last_name', 'email', 'status'
            ));

            if (auth()->user()->id != $user->id) {
                $user->roles()->sync($request->role);
            }
            session()->flash('success', trans('admin_lang.user_updated'));
            return [
                'response' => config('response.success'),
                'redirect' => route('admin.users.index')
            ];
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    public function destroy($user_id)
    {
        if (!auth()->user()->can('delete-users')) {
            abort(401);
        }
        try {
            $user = User::findOrFail($user_id);
            $user->delete();
            return [
                'response' => config('response.success'),
                'title' => trans('admin_lang.success'),
                'message' => trans('admin_lang.user_deleted'),
                'reload' => true
            ];
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'title' => trans('admin_lang.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'title' => trans('admin_lang.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    public function changeStatus(Request $request)
    {
        $user = User::findOrFail($request->userId);
//        if($request->status == User::ACTIVE && $user->email_verified_at == null){
//            $user->email_verified_at = Carbon::now();
//        }
        $user->status = $request->status;
        $user->save();
        return response([
            'status' => true,
            'message' => trans('admin_lang.user_status_changed_successfully'),
        ], 200);
    }

    public function avatar(Request $request)
    {
        try {
            $image = $request->file('file');
            $new_name = auth()->id() . '_avatar_img.' . $image->getClientOriginalExtension();

            $path = '/avatar/' . $new_name;

            Storage::put('public/' . $path, $image->getContent());

            $userProfile = User::where('id', auth()->id())->first();
            $userProfile->avatar = 'storage/' . $path;
            $userProfile->save();

            return response()->json([
                'status' => true,
                'message' => trans('admin_lang.image_uploaded_successfully'),
            ], 200);
        } catch (\Exception $e) {
            if (config('app.app_type') == 'local') {
                return [
                    'response' => config('response.error'),
                    'title' => trans('admin_lang.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'title' => trans('admin_lang.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    /**
     * @param Request $request
     * @param bool $is_update
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    private function __validate(Request $request, $is_update = false)
    {
        $unique_email_rule = 'unique:users,email';
        if ($is_update) {
            $unique_email_rule .= ',' . $is_update;
        }

        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|' . $unique_email_rule,
            'role' => ['required', Rule::in(Role::active()->pluck('id'))],
        ];

        if ($is_update) {
            unset($rules['password']);
            unset($rules['email']);
            $rules['status'] = ['required', Rule::in(array_column(User::$status, 'code'))];
            if (auth()->user()->id == $is_update) {
                unset($rules['role']);
            }
        }
        return $this->validate($request, $rules);
    }
}
