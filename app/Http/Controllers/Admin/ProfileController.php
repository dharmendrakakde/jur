<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function myProfile()
    {
        $id = auth()->id();
        $user = User::with('roles')->findOrFail($id);
        $roles = Role::active()->get();
        return view('admin.users.profile', compact('user','roles'));
    }

    public function updateProfile(Request $request,$user_id)
    {
        $user_id = auth()->id();
        $user = User::with('roles')->findOrFail($user_id);
        $request->validate([
            'name' => 'required|max:255',
            'address' => 'required',
            'city' => 'required|max:255',
            'state' => 'required|max:255',
            'country_id' => ['required'],
            'zip_code' => 'required|max:6|alpha_num',
            'phone' => 'required|regex:/[0-9]{10}/',
        ]);
        try {

            $user->update($request->only(
                'name', 'address', 'city',
                'state', 'country_id', 'zip_code', 'phone'
            ));

            session()->flash('success', trans('admin_lang.profile_updated'));
            return [
                'response' => config('response.success'),
                //'redirect' => route('admin.dashboard.index')
                'title' => trans('admin_lang.success'),
                'message' => trans('admin_lang.profile_updated'),
                'reload' => true
            ];
        } catch (\Exception $e) {
            if(config('app.app_type') == 'local'){
                return [
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ];
            }
            return [
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ];
        }
    }

    public function changePassword()
    {
        return view('admin.profile.change_password');
    }

    public function updatePassword(Request $request)
    {
        $this->__validate($request);
        try {
            $user = Auth::user();
            if(!Hash::check($request->current_password, $user->password)) {
                return response()->json([
                    'response' => config('response.error'),
                    'message' =>trans('admin_lang.password_not_match')
                ]);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            session()->flash('success', trans('admin_lang.password_update_successfully'));
            return response()->json([
                'response' => config('response.success'),
                'redirect' => route('admin.profile')
            ]);
        } catch (\Exception $e) {
            if(config('app.app_type') == 'local'){
                return response()->json([
                    'response' => config('response.error'),
                    'message' => $e->getMessage()
                ]);
            }
            return response()->json([
                'response' => config('response.error'),
                'message' => trans('admin_lang.something')
            ]);
        }
    }

    private function __validate(Request $request, $is_update = false)
    {

        $rules = [
            'password' => 'required|confirmed|min:8',
            'current_password' => 'required|min:8',
        ];
        return $this->validate($request, $rules);
    }
}
