<?php

namespace App\Traits;

use Carbon\Carbon;

trait WorkExperienceDateCheck
{
    public function dateValidate($request)
    {
        if ($request->start_date != "" && $request->end_date != "") {
            if (Carbon::now()->timestamp <= Carbon::parse($request->start_date)->timestamp) {
                session()->flash('error', trans('admin_lang.start_date_is_not_grater_than'));
                return [
                    'status' => false,
                    'response' => config('response.error'),
                    'message' => trans('admin_lang.start_date_is_not_grater_than'),
                ];
            } else if (Carbon::now()->timestamp <= Carbon::parse($request->end_date)->timestamp) {
                session()->flash('error', trans('admin_lang.end_date_is_not_grater_than_current'));
                return [
                    'status' => false,
                    'response' => config('response.error'),
                    'message' => trans('admin_lang.end_date_is_not_grater_than_current'),
                ];
            } else if (Carbon::parse($request->start_date)->timestamp > Carbon::parse($request->end_date)->timestamp) {
                session()->flash('error', trans('admin_lang.end_date_is_not_lesser_than'));
                return [
                    'status' => false,
                    'response' => config('response.error'),
                    'message' => trans('admin_lang.end_date_is_not_lesser_than'),
                ];
            }
        } else {
            if (Carbon::now()->timestamp <= Carbon::parse($request->start_date)->timestamp) {
                session()->flash('error', trans('admin_lang.start_date_is_not_grater_than'));
                return [
                    'status' => false,
                    'response' => config('response.error'),
                    'message' => trans('admin_lang.start_date_is_not_grater_than'),
                ];
            }
        }
        return [
            'status' => true
        ];
    }
}
