<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\Module;
use Illuminate\Database\Seeder;
use App\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operations = ['read', 'add', 'edit', 'delete'];

        foreach (config('permission.allowed_modules') as $module_slug => $module) {
            $module = Module::updateOrCreate([
                'name' => $module_slug,
            ], [
                'display_name' => json_encode($module)
            ]);

            //module permissions
            foreach ($operations as $operation) {
                Permission::updateOrCreate([
                    'name' => $operation . '-' . $module_slug,
                    'guard_name' => config('auth.defaults.guard'),
                    'module_id' => $module->id
                ], []);
            }
        }

        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        foreach (config('defaults.roles') as $slug => $role) {
            $new_role = Role::updateOrCreate([
                'name' => $role['name'],
                'guard_name' => config('auth.defaults.guard')
            ], [
                'display_name' => $role['display_name'],
            ]);
            $permissions = [];
            if (in_array('*', $role['permissions'])) {
                foreach (Permission::all() as $permission) {
                    if (!in_array($permission->name, $role['exclude_permissions'])) {
                        $permissions[] = $permission;
                    }
                }
            } else {
                $permissions = $role['permissions'];
            }
            $new_role->givePermissionTo($permissions);
        }
    }
}
