<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = config('defaults.admin');
        $user = User::updateOrCreate([
            'first_name' => $admin['first_name'],
            'last_name' => $admin['last_name'],
            'email' => $admin['email'],
            'email_verified_at' => date('Y-m-d'),
            'status' => User::$status[$admin['status']]['code']
        ], ['password' => Hash::make($admin['password'])]);

        $user->roles()->attach(Role::where('name', $admin['role'])->firstOrFail()->id);
    }
}
