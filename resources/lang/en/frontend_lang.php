<?php

return [

    //Header
    'bycost' => 'ByCost',
    'home' => 'Home',
    'get_off' => 'Get 15% off with Newsletter',
    'sign_up' => 'Sign up',
    'login_register' => 'Login/Register',
    'sell_on_bycost' => 'Sell on Bycost',
    'sell_on_tabak' => 'Sell on Tabak',
    'search_for_product' => 'Search for products, brands and more',
    'my_account' => 'My Account',
    'cart' => 'Shopping Cart',
    'category' => 'Category',
    'search_box_place_holder' => 'Search for products, brands and more',


    //Menu
    'cigarette' => 'Cigarette',
    'cigar' => 'Cigar',
    'pipe_tobacco' => 'Pipe Tobacco',
    'roll_y_own' => 'Roll Y Own',
    'shisha' => 'Shisha',
    'e_cig' => 'E-Cig',
    'pipes' => 'Pipes',
    'accessories' => 'Accessories',
    'gifts' => 'Gifts',
    

    //Home
    'best_sellers' => 'Best Sellers',
    'category_section' => 'Category Section',
    'special' => 'Special',
    'trendy_items' => 'Trending Items',
    'discover_top_picks_items' => 'Discover Top Picks Items',
    'discover_our_products' => 'Discover Our Products',
    'discover' => 'Discover',
    'browse_the_huge' => 'Browse the huge variety of our special products',
    'see_all_items' => 'See All Items',
    'clearance' => 'Clearance',
    'shop_now' => 'Shop Now',
    'select_from_your_favourite_brand' => 'Select From Your Favourite Brand',
    'get_special_services' => 'Get special services from our shop',
    'view_all' => 'View All',
    'recommended_for_you' => 'Recommended for you',
    'more_to_love' => 'More To Love',
    'newsletter' => 'Newsletter',
    'subscribe_here_to_get_update' => 'Subscribe here to get every single updates',
    'enter_your_email' => 'Enter your Email Address....',
    'subscribe_now' => 'Subscribe Now',
    'recent_view' => 'Your Recent Viewed',


    //Footer
    'copyright' => 'Copyright By @ByCost',
    'help_center' => 'Help Center',
    'policy' => 'Policy',
    'about_us' => 'About Us',
    'cooperate_with_us' => 'Cooperate With Us',
    'delivery' => 'Delivery',
    'how_to_order' => 'How to order',
    'faq' => 'FAQ',
    'about_payment' => 'About Payment',
    'maintenance_policy' => 'Maintenance Policy',
    'the_3c_warranty_policy' => 'The 3C Warranty Policy',
    'return_policy' => 'Return Policy',
    'terms_and_conditions' => 'Terms and Conditions',
    'delivery_service_introduce' => 'Delivery Service Introduce',
    'intellectual_property_statement' => 'Intellectual Property Statement',
    'about_bycost' => 'About ByCost',
    'contact_us' => 'Contact Us',
    'why_sell_on_bycost' => 'Why sell on ByCost?',
    'browse_by_category' => 'Browse by category',
    'get_delivery' => 'Get Delivery',
    'shopping_with_us' => 'Shopping with us',
    'making_payments' => 'Making payments',
    'delivery_options' => 'Delivery options',
    'buyer_protection' => 'Buyer Protection',
    'customer_service' => 'Customer service',
    'transaction_services' => 'Transaction Services',
    'agreement' => 'Agreement',
    'take_our_feedback' => 'Take our feedback',
    'help_service' => 'Help Service',
    'disputes_reports' => 'Disputes & Reports',
    'report_ipr_infringement' => 'Report IPR infringement',
    'send' => 'Send',
    'google_play' => 'Google Play',
    'apple_store' => 'Apple Store',
    'coming_soon_app' => 'Coming Soon',
    'await_to_experience' => 'Await to experience the best things',


    //Product Listing
    'product_listing' => 'Product Listing',
    'categories' => 'Categories',
    'variant' => 'Variant',
    'filter' => 'Filter',
    'clear_all' => 'clear all',
    'price_range' => 'Price Range',
    'attribute' => 'Attribute',
    'brands' => 'Brands',
    'no_attributes_found' => 'No attributes found',
    'no_categories_found' => 'No categories found',
    'showing_results_for' => 'Showing results for',
    'products' => 'Products',
    'all_product' => 'All Product',
    'bycost_mobile_app' => 'ByCost Mobile App',
    'search_any_where' => 'Search Anywhere, Anytime',
    'scan_or_click' => 'Scan or Click to download',


    //Product Checkout
    'checkout' => 'Checkout',
    'your_items' => 'Your items',
    'remove' => 'Remove',
    'order_summary' => 'Order Summary',
    'items_total' => 'Item(s) Total',
    'total_price' => 'Total Price',
    'coupon_code' => 'Coupon code...',
    'cash_on_delivery' => 'Cash On Delivery',
    'Purchase' => 'Purchase',
    'purchase' => 'Purchase',
    'add_gift_code' => 'Add Gift Code',

    
    
    'your_items' => 'Your items',
    'remove' => 'Remove',
    'order_summary' => 'Order Summary',
    'items_total' => 'Item(s) Total',
    'total_price' => 'Total Price',
    'coupon_code' => 'Coupon code...',
    'continue_shopping' => 'Continue Shopping',
    'item_in_cart' => 'item in cart',
    'shopping_cart' => 'Shopping Cart',



    //Product Detail
    'product_detail' => 'Product Detail',
    'qty' => 'Quantity',
    'description' => 'Description',
    'related_products' => 'Related Products',
    'share' => 'Share',
    'products' => 'Products',
    'choose_variant' => 'Choose Variant',
    'read_more' => 'Read More',
    'thank_you_for_question' => 'Thank you for question, we will contact you soon',
    'post_your_question' => 'Post your question',
    'your_question_may_be' => 'Your question may be answered by sellers, manufacturers, or customers who purchased this item, who are all part of the ByCost community',
    'post' => 'Post',
    'question_place_holder' => 'Please enter a question',
    'by' => 'By',
    'customer_question' => 'Customer questions & answers',
    'question_qa' => 'Question:',
    'answer_qa' => 'Answer:',
    'bsin' => 'BSIN',
    'upc' => 'UPC',
    'non_returnable' => 'Non Returnable',
    'not_available_for_this_country' => 'Not available for this country',
    'does_not_ship_to' => 'Does not ship to',

    //Cart
    'product_not_found' => 'Product Not Found',
    'product_added_to_cart' => 'Product added to cart successfully!',
    'product_remove_successfully' => 'Product remove successfully!',
    'no_item_in_cart' => 'No item in cart',
    'add_to_cart' => 'Add To Cart',
    'size_photos' => 'Size Photos',
    'customer_reviews' => 'Customer Reviews',
    'q_a' => 'Q&A',
    'ship_from' => 'Ship From',
    'free_return' => 'Free Return',
    'free_shipping' => 'Free Shipping',
    'specification' => 'Specification',
    'reviews' => 'Reviews',
    'load_more' => 'Load More',
    'maximum_qty_limit' => 'Max QTY limit is :qty',
    'max_qty_limit' => 'Max QTY limit is',


    //Login
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'welcome' => 'Welcome ',
    'firstname' => 'First Name',
    'lastname' => 'Last Name',
    'email_address' => 'Email Address',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'username_email_address' => 'Username or email address',
    'forgot' => 'I forgot',
    'forgot_password' => 'Forgot Password?',
    'my_account' => 'My Account',
    'signup' => 'Sign Up',

    'user_registered' => 'User registered successfully.',
    'user_register_failed' => 'Failed to register a user.',
    'user_updated' => 'User updated successfully.',
    'user_update_failed' => 'Failed to update user.',
    'something' => 'Something went wrong',
    'invalid_details' => 'Invalid Credentials.',
    'register_as' => 'Register As',
    'vendor' => 'Vendor',
    'customer' => 'Customer',
    'vendor_info' => 'Vendor Info',
    'tier_placeholder' => 'Tier',
    'country_placeholder' => 'Select Country',
    'currency_placeholder' => 'Select Currency',
    'username_placeholder' => 'Username',
    'email_placeholder' => 'Email',
    'phone_placeholder' => 'Phone',
    'fullname_placeholder' => 'Full Name',
    'name_placeholder' => 'Name',
    'address1_placeholder' => 'Address 1',
    'address2_placeholder' => 'Address 2',
    'city_placeholder' => 'City',
    'state_placeholder' => 'State',
    'zip_placeholder' => 'ZIP Code',
    'logo_placeholder' => 'Logo',

    'profile' => 'Profile',
    'change_password' => 'Change Password',
    'firstname_placeholder' => 'First Name',
    'lastname_placeholder' => 'Last Name',
    'address_placeholder' => 'Address',
    'update' => 'Update',
    'user_profile' => 'Profile Details',
    'vendor_profile' => 'Vendor Profile',
    'user_profile_updated' => 'User Profile updated successfully.',
    'vendor_profile_updated' => 'Vendor Profile updated successfully.',
    'success' => 'Success',
    'warning' => 'Warning',
    'error' => 'Error',
    'old_password' => 'Old Password',
    'new_password' => 'New Password',
    'update_password' => 'Update Password',
    'invalid_oldpassword' => 'Invalid Old Password',
    'user_password_changed' => 'Password is updated',
    'vendor_register' => 'Vendor Registration',
    'shop_name' => 'Shop Name',
    'full_name' => 'Full Name',
    'username' => 'User Name',
    'question_and_answer' => 'QUESTIONS & ANSWERS',
    'question' => 'Question',
    'post_date' => 'Post Date',
    'view' => 'View',
    'at' => 'at',
    'type_something' => 'Type Something',
    'please_enter_msg' => 'Please enter msg',
    'product_information' => 'Product information',
    'no_information_found' => 'No information found',
    'date' => 'date',
    'type' => 'Type',
    'support' => 'Support',
    'id' => 'ID',
    'product_image' => 'Product Image',
    'manage_address' => 'Manage Address',
    'add _new' => 'Add New',


    //Order
    'order_management' => 'Order Management',

    'add_new_address' => 'Add New Address',
    'select_address' => 'Select Address',
    'zip_code' => 'Zip',
    'phone_no' => 'Phone No',
    'mobile_no' => 'Phone No',
    'alternate_mobile_no' => 'Alternate Phone No',
    'select_country' => 'Select Country',
    'state' => 'State',
    'city' => 'City',
    'landmark' => 'Landmark',
    'street_address' => 'Street Address',
    'block_no' => 'Block No/House No',
    'title' => 'Title',
    'select_type' => 'Select Type',
    'out_of_stock' => 'Out Of Stock',
    'action' => 'Action',
    'upload_at_least_one_image' => 'Upload at least one image',
    'status_full_return' => 'Full Return',
    'order_item_return_initiated' => 'Item return request initiated',
    'order_item_cancel_initiated' => 'Item cancel request initiated',
    'support_initiated' => 'Support request initiated',
    'support_initiated_successfully' => 'Support request initiated successfully',
    'comment_place_holder' => 'Write your comment here',
    'no_data_found' => 'No data found',

    'verify_email' => 'Verify Email',
    'comment' => 'Comment',
    'select_terms_and_condition' => 'Please select terms & conditions',
    'select_at_least_one_reason' => 'Please select at least one reason',
    'please_write_comment' => 'Please write comment',
    'reason_place_holder' => 'Comment your reason here...',
    'i_agree_return_policy' => 'I agree to the Terms and condition of return policy',
    'submit' => 'Submit',
    'why_return_order' => 'Why do you want to return this order',

    'no_longer_needed' => 'No longer needed',
    'missed_estimated_delivery_date' => 'Missed estimated delivery date',
    'different_from_what_was_ordered' => 'Different from what was ordered',
    'missing_parts_or_accessories' => 'Missing parts or accessories',
    'defective_does_not_work_properly' => 'Defective / Does not work properly',
    'item_in_damaged_condition' => 'Item in damaged condition',
    'others' => 'Others',

    // user orders
    'no_client_orders' => 'You have not placed any order yet.',
    'your_orders' => 'Your Orders',
    'search_all_orders' => 'Search all Orders',
    'search' => 'Search',
    'orders' => 'Orders ',
    'placed_in' => 'Placed in',

    'list_3_months' => 'Last 3 months',
    'list_6_months' => 'Last 6 months',

    'order_id' => 'Order ID',
    'order_placed' => 'Order Placed',
    'store_name' => 'Store Name',
    'order_details' => 'Order Details',
    'invoice' => 'Invoice',
    'order_amount' => 'Order Amount',
    'product' => 'Product',
    'price' => 'Price',
    'total' => 'Total',

    'cancel_order' => 'Cancel Order',
    'return_order' => 'Return Order',

    'see_more' => 'See more',
    'coupon' => 'Coupon',
    'wish_list' => 'Wish List',
    'viewed' => 'Viewed',
    'welcome_to_bycost' => 'Welcome To ByCost',
    'join' => 'Join',
    'flash_deals' => 'Flash Deals',
    'view_more' => 'View More',
    'off' => 'Off',
    'top_selection' => 'Top Selections',
    'new_arrivals' => 'New Arrivals',
    'featured_brands' => 'Featured Brands',
    'featured_category' => 'Featured Categories',
    'stores_you_will_love' => 'Stores You Will Love',

    'order_info' => 'Order Info',
    'order_details' => 'Order Details',
    'order_on' => 'Ordered On',
    'order_id' => 'Order ID',
    'order_status' => 'Status',
    'order_item_subtotal' => 'Item(s) Subtotal',
    'order_shipping' => 'Shipping',
    'order_discount_applied' => 'Promotion Applied',
    'order_grand_total' => 'Grand Total',
    'contact_info' => 'Contact Info',
    'name' => 'Name',
    'email' => 'Email',
    'phone_no' => 'Phone No.',
    'addresses' => 'Addresses',
    'billing_address' => 'Billing Address',
    'shipping_address' => 'Shipping Address',

    'confirm_button_no_text' => 'No, leave it.',
    'confirm_button_yes_text' => 'Yes, do it.',
    'confirm_order_cancel_title' => 'Cancel Order',
    'confirm_order_cancel_text' => 'Are you sure to cancel items from this order?',
    'confirm_order_return_title' => 'Return Order',
    'confirm_order_return_text' => 'Are you sure to return items from this order?',
    'confirm_order_cancel_select' => 'Select at least one item to cancel',
    'confirm_order_return_select' => 'Select at least one item to return',
    'confirm_button_ok_text' => 'OK',
    'order_cancel_items' => 'Cancel Items',
    'order_items_canceled' => 'Items canceled from order',
    'order_items_canceled_not_found' => 'Invalid items for cancellation',
    'item_canceled' => 'Canceled',
    'item_returned' => 'Returned',
    'order_return_items' => 'Return Items',
    'item_return' => 'Return Item',
    'item_cancel' => 'Cancel Item',
    'order_items_returned' => 'Return item request is received',
    'order_items_returned_not_found' => 'Invalid items for return',
    'order_unable_to_cancel' => 'Unable to cancel items',
    'order_unable_to_return' => 'Unable to return items',
    'order_invoice' => 'Order Invoice',
    'order_number' => 'Order Number',
    'order_total' => 'Order Total',
    'payment_status' => 'Payment Status',
    'payment_paid' => 'Paid',
    'payment_not_paid' => 'Not Paid',
    'payment_mode' => 'Payment Mode',
    'payment_mode_cod' => 'Cash on Delivery',

    'results' => 'Results',

    'my_orders' => 'My Orders',
    'my_profile' => 'My Profile',
    'my_addresses' => 'Addresses',
    'add' => 'Add',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'country' => 'Country',
    'type' => 'Type',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'user_address_added' => 'Address added successfully.',
    'user_address_updated' => 'تم تحديث العنوان بنجاح.',
    'invalid_address' => 'Address not found',
    'address_deleted' => 'Address deleted successfully...',
    'confirm_delete_address_title' => 'Delete Address',
    'confirm_delete_address_text' => 'Are you sure to delete this address?',


    'no_result' => 'No result...',

    'account' => 'Account',
    'hello' => 'Hello',
    'login_as' => 'Login As',

    'ship_to' => 'Ship To',
    'buy_now' => 'Buy Now',
    'low_to_high' => 'Price: Low to High',
    'high_to_low' => 'Price: High to Low',
    'latest' => 'Latest',
    'sort_by' => 'Sort By',

    'empty_cart' => 'Your shopping cart is empty',
    'add_items' => 'Add items to your cart',

    'confirm_remove_cart_item' => 'Remove Item',
    'confirm_remove_cart_text' => 'Are you sure to remove this item from cart?',
    'edit_orders' => 'Edit Orders',
    'subtotal' => 'Subtotal',
    'user_address_changed' => 'Shipping address updated',
    'use_address' => 'Use Address',
    'save_use_address' => 'Save & Use Address',
    'shipping_details' => 'Shipping Details',
    'payment_details' => 'Payment Details',
    'change_address' => 'Change Address',
    'add_shipping_address' => 'Add Shipping Address',
    'no_default_address' => 'No address found for shipping.<br />Please provide shipping address',
    'product_name' => 'Product Name',
    'grand_total' => 'Grand Total',
    'shipping' => 'Shipping',

    'register_title' => 'We are as excited as you are to have you on board !',
    'login_title' => 'Happiness awaits on the other side!',
    'sign_in' => 'Sign In',
    'sort_by' => 'Sort By',

    //vendor-store
    'vendor_store' => 'Vendor Store',
    'store' => 'Store',
    'since' => 'Since',
    'vendor_product_categories' => 'Vendor Products Categories',
    'store_product_search' => 'Search products in store',
    'product_choose_from' => 'products to choose from',
    'sales' => 'Sales',
    'view_all_products' => 'View All Products',

    //home
    'language' => 'Language',
    'currency' => 'Currency',
    'usa' => 'USA',
    'bahrain' => 'Bahrain',
    'english' => 'English',
    'arabic' => 'عربى',
    'shop_sales_now' => 'Shop sales now',
    'sales_ends_in' => 'Sales ends in 1 day',
    'caldwell_store' => 'Caldwell Store',
    'select_currency' => 'Select Currency',
    'usd' => 'USD',

    //footer
    'genuine_products' => 'Genuine Products',
    'shop_with_confidence' => 'Shop with confidence as we bring to you the best products.',
    'secured_payment' => 'Secured Payment',
    'pay_securely' => 'Pay securely with your preferred payment method.',
    'shipping' => 'Shipping',
    'we_partner' => 'We partner with best to deliver your orders on time.',
    'return_refund' => 'Returns / Refunds',
    'we_care' => 'We care for you with our return and refund policies',
    'support' => 'Support',
    'reach_out' => 'Reach out to us for any query and we shall address them at priority',
    'mobile_apps' => 'Mobile Apps',
    'coming_soon' => 'Coming soon to allow you to search, browse and order from anywhere anytime.',
    'all_rights_reserved' => 'bycost.com. All rights reserved.',
    'guest' => 'Guest',
    'personal_info' => 'Personal Information',
    'password_info' => 'Password must be at least 6 characters long, contain numbers and letters, and must not match your name and email.',

    'edit_address' => 'Edit Address',
    'details' => 'Details',
    'address_book' => 'Address Book',
    'wishlist' => 'Wishlist',
    'account_overview' => 'Account Overview',
    'account_details' => 'Account Details',
    'default_address' => 'Default Address',
    'make_default' => 'Make Default',
    'no_address_found' => 'Address not found!',
    'default_address_changed' => 'Default address changed',
    'valid_coupon' => 'Coupon is Valid',

    'wishlist_item_added'=> 'Item added to Wishlist',
    'wishlist_item_removed'=> 'Item removed from Wishlist',
    'item_unavailable' => 'Not available...',
    'dashboard' => 'Dashboard',
    'move_to_wishlist' => 'Move to Wishlist',
    'add_new' => 'Add New',
    'moved_to_wishlist' => 'Item moved to Wishlist',
    'already_wishlisted' => 'Item is already Wishlisted',

    'free_coupons' => 'Free Coupons',
    'promo_code' => 'Promo Code',
    'no_coupons' => 'No coupons available for you.',
    'no_items_in_wishlist' => 'No item in Wishlist',
    'days_replacement' => ':days Days Replacement',
    'reset_password' => 'Reset Password',
    'mail_sent' => 'Mail sent successfully.',
    'mail_failed' => 'Failed to send email.',
    'tabak' => 'Tabak Depot',

    //Checkout
    'choose_shipping' => 'Choose shipping',
    'standard' => 'Standard',
    'express' => 'Express',
    'including_shipping' => ' with shipping included',
    'no_deliverable_item_in_cart' => 'No deliverable item in cart',
    'dob' => 'Date Of Birth',

    //Age Restrictions
    'we_have_been' => 'We have been trading in fine tobacco and smokers\' accessories and there are some products that will require to verify your age',
    'are_you' => 'Are you :age or Older?',
    'i_m_over' => 'I\'M OVER :age',



]
?>