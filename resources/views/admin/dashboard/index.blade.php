@extends('layouts.admin_main')

@section('title')
    @lang('admin_lang.dashboard')
@endsection

@section('css')
@endsection

@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <h1>Hello {{ auth()->user()->name }}</h1>
    </div>
@endsection

