@extends('layouts.admin_main')

@section('title', trans('admin_lang.users'))

@section('content')
    @breadcrumb([
        'home_url' => route('admin.dashboard.index'),
        'links' => [
            [
                'url' => route('admin.users.index'),
                'title' => trans('admin_lang.users_management'),
            ]
        ],
        'current_page_title' => trans('admin_lang.view_user')
        ])
    @endbreadcrumb
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                @header(['title' => trans(('admin_lang.view_user')), 'icon' => 'fa fa-user-friends'])
                @endheader
                <!--begin::Form-->
{{--                    <form class="kt-form" action="{{ route('admin.users.update', $user->id) }}" method="PATCH">--}}
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="first-name" class="required">@lang('admin_lang.first_name')</label>
                                        <input id="first-name" type="text" name="first_name" placeholder="{{ trans('admin_lang.first_name_placeholder') }}" required class="form-control" value="{{ $user->first_name }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="last-name" class="required">@lang('admin_lang.last_name')</label>
                                        <input id="last-name" type="text" name="last_name" placeholder="{{ trans('admin_lang.last_name_placeholder') }}" required class="form-control" value="{{ $user->last_name }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="username" class="required">@lang('admin_lang.username')</label>
                                        <input id="username" value="{{ $user->username }}" type="text" name="username" placeholder="{{ trans('admin_lang.username_placeholder') }}" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email" class="required">@lang('admin_lang.email')</label>
                                        <input id="email" type="email" name="email" placeholder="{{ trans('admin_lang.email_placeholder') }}" required class="form-control" value="{{ $user->email }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="address" class="required">@lang('admin_lang.address')</label>
                                        <textarea id="address" name="address" class="form-control" placeholder="{{ trans('admin_lang.address_placeholder') }}" readonly>{{ $user->address }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="city" class="required">@lang('admin_lang.city')</label>
                                        <input id="city" type="text" name="city" placeholder="{{ trans('admin_lang.city_placeholder') }}" required class="form-control" value="{{ $user->city }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="state" class="required">@lang('admin_lang.state')</label>
                                        <input id="state" type="text" name="state" placeholder="{{ trans('admin_lang.state_placeholder') }}" required class="form-control" value="{{ $user->state }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="country" class="required">@lang('admin_lang.select_country')</label>
                                        <select id="country" name="country_id" class="form-control" disabled>
                                            <option value="">@lang('admin_lang.select_country')</option>
                                            @foreach($countries as $key => $country)
                                                <option value="{{ $key }}" {{ $user->country_id == $key ? 'selected' : '' }}>{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="zip-code" class="required">@lang('admin_lang.zip_code')</label>
                                        <input id="zip-code" type="text" name="zip_code" placeholder="{{ trans('admin_lang.zip_code_placeholder') }}" required class="form-control" value="{{ $user->zip_code }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="phone" class="required">@lang('admin_lang.phone')</label>
                                        <input id="phone" type="text" name="phone" placeholder="{{ trans('admin_lang.phone_placeholder') }}" required class="form-control" value="{{ $user->phone }}" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="role" class="required">@lang('admin_lang.select_role')</label>
                                        <select id="role" name="role" class="form-control" disabled>
                                            <option value="">@lang('admin_lang.select_role')</option>
                                            @foreach($roles as $key => $role)
                                                <option value="{{ $role->id }}" {{ $user->hasRole($role->name) ? 'selected' : ''}}>{{ $role->display_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="status" class="required">@lang('admin_lang.status')</label>
                                        <select id="status" name="status" class="form-control" disabled>
                                            <option value="">@lang('admin_lang.select_status')</option>
                                            @foreach(\App\User::$status as $key => $status)
                                                <option value="{{ $status['code'] }}" {{ $user->status == $status['code'] ? 'selected' : ''}}>{{ trans('admin_lang.status_'.$status['slug']) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                {{--@button(['type' => 'submit'])@endbutton--}}
                                @button(['type' => 'cancel', 'url' => url()->previous()])@endbutton
                            </div>
                        </div>
                    {{--</form>--}}
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection
