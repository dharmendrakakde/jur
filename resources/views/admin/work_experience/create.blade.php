@extends('layouts.admin_main')

@section('title', trans('admin_lang.create_new_work_experience'))

@section('content')
    <!-- begin:: Subheader -->
    @include('components.breadcrumb',[
        'home_url' => route('admin.dashboard.index'),
        'links' => [
            [
                'url' => route('admin.work-experiences.index'),
                'title' => trans('admin_lang.work_experience_management'),
            ]
        ],
        'current_page_title' => trans('admin_lang.work_experience_create')
    ])
    <!-- end:: Subheader -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    @include('components.header',['title' => trans('admin_lang.create_new_work_experience'), 'icon' => 'fas fa-hotel'])
                    <!--begin::Form-->
                    <form class="kt-form" action="{{ route('admin.work-experiences.store') }}" method="POST">
                        <input type="hidden" id="is_same_role" name="is_same_role" value="0">
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="company_name" class="required">@lang('admin_lang.company_name')</label>
                                        <input id="company_name" type="text" name="company_name" placeholder="{{ trans('admin_lang.company_name_placeholder') }}" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="title" class="required">@lang('admin_lang.title')</label>
                                        <input id="title" type="text" name="title" placeholder="{{ trans('admin_lang.title_or_role_placeholder') }}" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="start_date" class="required">@lang('admin_lang.start_date')</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" readonly  placeholder="{{ trans('admin_lang.start_date_placeholder') }}"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <label class="checkbox">
                                            <input type="checkbox" id="is_same_role_checkbox" name="is_same_role_checkbox" value="0" title="{{ trans('admin_lang.same_role') }}" style="margin-top: 10px;">
                                            <span>{{ trans('admin_lang.same_role') }}</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group" id="end_date_div">
                                        <label for="end_date" class="required">@lang('admin_lang.end_date')</label>
                                        <div class="input-group date">
                                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" readonly  placeholder="{{ trans('admin_lang.end_date_placeholder') }}"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="description">@lang('admin_lang.description')</label>
                                        <textarea id="description" name="description" placeholder="{{ trans('admin_lang.description_placeholder') }}" required class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                @include('components.button',['type' => 'submit'])
                                @include('components.button',['type' => 'cancel', 'url' => route('admin.work-experiences.index')])
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
    {{--<script src="{{ asset('assets/js/pages/crud/forms/widgets/form-repeater.js') }}"></script>--}}
    <script>
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy'
        });
        $(document).ready(function() {
            $('#is_same_role_checkbox').change(function() {
                if($("#is_same_role_checkbox").prop('checked') == true){
                    $('#end_date').val('');
                    $('#is_same_role_checkbox').val(1);
                    $('#is_same_role').val(1);
                    $('#end_date_div').addClass('d-none');
                }
                else{
                    $('#is_same_role_checkbox').val(0);
                    $('#is_same_role').val(0);
                    $('#end_date_div').removeClass('d-none');
                }
            });
        });
    </script>
@endpush
