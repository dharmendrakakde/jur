<?php
    $edit = [
        'edit' => [
            'url' => route("admin.work-experiences.edit", $workExperience->id)
        ],
    ];
    $show = [
//        'show' => [
//            'url' => route("admin.work-experiences.show", $workExperience->id)
//        ],
    ];
    $delete = [
        'delete' => [
            'url' => route("admin.work-experiences.destroy", $workExperience->id),
            'class' => 'btn-delete',
            'swal_desc' => ' ',
            'swal_confirm' => trans('admin_lang.delete_work_experience_swal_confirm'),
            'swal_cancel' => trans('admin_lang.delete_user_swal_cancel'),
            'swal_title' => trans('admin_lang.delete_work_experience_swal_title')
        ]
    ];
?>

@include('components.dropdown',['links' => array_merge($edit, $delete, $show)])
