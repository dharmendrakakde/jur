@extends('layouts.admin_main')

@section('title', trans('admin_lang.roles'))

@section('content')
    @include('components.breadcrumb',[
        'home_url' => route('admin.dashboard.index'),
        'links' => [
            [
                'url' => route('admin.roles.index'),
                'title' => trans('admin_lang.rolesManagement'),
            ]
        ],
        'current_page_title' => trans('admin_lang.edit_role')
    ])
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    @include('components.header',['title' => trans('admin_lang.update_role'), 'icon' => 'fa fa-user-tie'])
                    <!--begin::Form-->
                    <form class="kt-form" action="{{ route('admin.roles.update', $role->id) }}" method="PATCH">
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="display_name" class="required">@lang('admin_lang.display_name')</label>
                                        <input type="text" name="display_name[en]" value="{{ $role->getTranslation('display_name', 'en') }}" id="display_name" placeholder="{{ trans('admin_lang.role_display_name_placeholder') }}" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <div class="d-flex align-items-center">
                                            <label for="status">@lang('admin_lang.status')</label>
                                            <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--info ml-auto">
                                                <label>
                                                    <input type="checkbox" {{ $role->status ? 'checked' : '' }} id="status" value="1" name="status" required>
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1"></div>
                            </div>

                            <label for="modules">@lang('admin_lang.give_permission_label')</label>

                            <div class="form-group row">
                                <div class="col-12">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width: 17%;">
                                                <a class="permission-select-all" href="javascript:void(0);">@lang('admin_lang.str_select_all')</a>
                                                |
                                                <a class="permission-deselect-all" href="javascript:void(0);">@lang('admin_lang.str_deselect_all')</a>
                                            </th>
                                            <th class="text-center">@lang('admin_lang.str_read')</th>
                                            <th class="text-center">@lang('admin_lang.str_add')</th>
                                            <th class="text-center">@lang('admin_lang.str_edit')</th>
                                            <th class="text-center">@lang('admin_lang.str_delete')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($modules as $module)
                                            <tr>
                                                <td>@lang('admin_lang.module_'.$module)</td>
                                                @foreach($permissions[$module] as $permission)
                                                    <td class="text-center">
                                                        <label class="checkbox">
                                                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" @if(in_array($permission->id, $existingPermissions)) checked @endif title="{{ $permission->name }}">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                @include('components.button',['type' => 'update'])
                                @include('components.button',['type' => 'cancel', 'url' => route('admin.roles.index')])
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('document').ready(function () {
            $('.permission-select-all').on('click', function(){
                $('.table').find("input[type='checkbox']").prop('checked', true);
            });

            $('.permission-deselect-all').on('click', function(){
                $('.table').find("input[type='checkbox']").prop('checked', false);
                return false;
            });
        });
    </script>
@endsection
