<a href="{{ route('admin.roles.index', ['status' => $roles->status]) }}">
    @if ($roles->status == \App\Models\Role::$status['active']['code'])
        <span class="kt-badge kt-badge--inline status-active"> {{ trans('admin_lang.status_active') }} </span>
    @else
        <span class="kt-badge kt-badge--inline status-inactive">{{ trans(('admin_lang.status_inactive')) }}</span>
    @endif
</a>
