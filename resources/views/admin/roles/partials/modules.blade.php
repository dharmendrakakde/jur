@forelse ($modules as $module)
    <a href="{{ route('admin.roles.index',[\App\Models\Role::$filter_keys['module'] => $module->name]) }}"><span class="kt-badge kt-badge--inline module-{{ $module->name }}">{{ translate($module->display_name) }}</span></a>
@empty
    @lang('admin_lang.no_modules')
@endforelse
