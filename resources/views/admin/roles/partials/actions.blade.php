@if (!in_array($roles->name, ['admin']) && !auth()->user()->hasRole($roles->name))
    @include('components.dropdown',[
        'links' => [
            'edit' => [
                'url' => route("admin.roles.edit", $roles->id)
            ],
        ]
    ])
@else
@endif
