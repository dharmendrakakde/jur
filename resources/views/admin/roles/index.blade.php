@extends('layouts.admin_main')

@section('title', trans('admin_lang.roles'))

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        @include('components.header',['title' => trans('admin_lang.rolesManagement'), 'icon' => 'fa fa-user-tie'])
        <div class="kt-portlet__body">
            <div class="table_container">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="roles-table">
                        <thead>
                            <tr>
                                <th>@lang('admin_lang.id')</th>
                                <th>@lang('admin_lang.title')</th>
                                <th>@lang('admin_lang.users')</th>
                                <th>@lang('admin_lang.modules')</th>
                                <th>@lang('admin_lang.status')</th>
                                <th>@lang('admin_lang.actions')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script type="text/template" id="header-buttons">
{{--    @include('components.button',['type' => 'create', 'label' => trans('admin_lang.create_new_role'), 'url' => route('admin.roles.create')])--}}
</script>

<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    const url = "{{ route('admin.roles.data') }}";
    const lang = "{{ app()->getLocale() }}";
    const filters = {!! json_encode($filters) !!};

    jQuery(document).ready(function() {
        dataTable(jQuery('#roles-table'), url, [
                { data: 'index', defaultContent: '' },
                { data: 'display_name', name: 'display_name' },
                { data: 'users', name: 'users', searchable: false, orderable: false },
                { data: 'modules', name: 'modules', searchable: false, orderable: false },
                { data: 'status', name: 'status', orderable: true, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            lang,
            filters,
            jQuery('#header-buttons').html()
        );
    });
</script>
@endpush
