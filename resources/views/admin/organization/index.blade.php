@extends('layouts.admin_main')

@section('title')
    @lang('admin_lang.organization_management')
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        @include('components.header',['title' => trans('admin_lang.organization_management'), 'icon' => 'fas fa-hotel'])
        <div class="kt-portlet__body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="organization-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('admin_lang.organization_name')</th>
                        <th>@lang('admin_lang.title_or_role')</th>
                        <th>@lang('admin_lang.start_date')</th>
                        <th>@lang('admin_lang.end_date')</th>
                        <th>@lang('admin_lang.created_at')</th>
                        <th>@lang('admin_lang.actions')</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/template" id="header-buttons">
    @include('components.button',['type' => 'create', 'label' => trans('admin_lang.create_new_organization'), 'url' => route('admin.organization.create')])
</script>

<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        dataTable(jQuery('#organization-table'), "{{ route('admin.organization.data') }}", [
                { data: 'index', defaultContent: '', orderable: false, searchable: false },
                { data: 'name', name: 'name', searchable: true, orderable: true },
                { data: 'title', name: 'title', searchable: true, orderable: true },
                { data: 'start_date', name: 'start_date', searchable: false, orderable: true  },
                { data: 'end_date', name: 'end_date', searchable: false, orderable: true  },
                { data: 'created', name: 'created_at', searchable: false, orderable: true  },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            "{{ app()->getLocale() }}",
            "",
            jQuery('#header-buttons').html(),
            undefined,
            [[4, 'desc']]
        );
    });
</script>
@endsection
