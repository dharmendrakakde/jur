<?php
    $edit = [
        'edit' => [
            'url' => route("admin.organization.edit", $organization->id)
        ],
    ];
    $show = [
//        'show' => [
//            'url' => route("admin.organization.show", $organization->id)
//        ],
    ];
    $delete = [
        'delete' => [
            'url' => route("admin.organization.destroy", $organization->id),
            'class' => 'btn-delete',
            'swal_desc' => ' ',
            'swal_confirm' => trans('admin_lang.delete_organization_swal_confirm'),
            'swal_cancel' => trans('admin_lang.delete_user_swal_cancel'),
            'swal_title' => trans('admin_lang.delete_work_experience_swal_title')
        ]
    ];
?>

@include('components.dropdown',['links' => array_merge($edit, $delete, $show)])
