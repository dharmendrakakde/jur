@extends('layouts.admin_main')

@section('title', trans('admin_lang.users'))

@section('content')
    @include('components.breadcrumb',[
        'home_url' => route('admin.dashboard.index'),
        'links' => [
            [
                'url' => route('admin.users.index'),
                'title' => trans('admin_lang.users_management'),
            ]
        ],
        'current_page_title' => trans('admin_lang.user_edit')
        ])
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    @include('components.header',['title' => trans(('admin_lang.update_user')), 'icon' => 'fa fa-user-friends']);
                    <!--begin::Form-->
                    <form class="kt-form" action="{{ route('admin.users.update', $user->id) }}" method="PATCH">
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="first-name" class="required">@lang('admin_lang.first_name')</label>
                                        <input id="first-name" type="text" name="first_name" placeholder="{{ trans('admin_lang.first_name_placeholder') }}" required class="form-control" value="{{ $user->first_name }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="last-name" class="required">@lang('admin_lang.first_name')</label>
                                        <input id="last-name" type="text" name="last_name" placeholder="{{ trans('admin_lang.last_name_placeholder') }}" required class="form-control" value="{{ $user->last_name }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email" class="required">@lang('admin_lang.email')</label>
                                        <input id="email" type="email" name="email" placeholder="{{ trans('admin_lang.email_placeholder') }}" required class="form-control" value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="role" class="required">@lang('admin_lang.select_role')</label>
                                        <select id="role" name="role" class="form-control" {{ auth()->user()->id == $user->id ? 'disabled' : '' }}>
                                            <option value="">@lang('admin_lang.select_role')</option>
                                            @foreach($roles as $key => $role)
                                                <option value="{{ $role->id }}" {{ $user->hasRole($role->name) ? 'selected' : ''}}>{{ $role->display_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="status" class="required">@lang('admin_lang.status')</label>
                                        <select id="status" name="status" class="form-control">
                                            <option value="">@lang('admin_lang.select_status')</option>
                                            @foreach(\App\Models\User::$status as $key => $status)
                                                <option value="{{ $status['code'] }}" {{ $user->status == $status['code'] ? 'selected' : ''}}>{{ trans('admin_lang.status_'.$status['slug']) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                @include('components.button',['type' => 'update'])
                                @include('components.button',['type' => 'cancel', 'url' => route('admin.users.index')])
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
<script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js') }}"></script>
@endpush
