@foreach($roles as $role)
    @if ($role->status == \App\Models\Role::$status['active']['code'])
        <a href="{{ route('admin.users.index', [\App\Models\User::$filter_keys['role'] => $role->name]) }}"><span class="kt-badge kt-badge--inline role-{{ $role->name }}">{{ $role->display_name }}</span></a>
    @endif
@endforeach
