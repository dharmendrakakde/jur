<?php
    $edit = [
        'edit' => [
            'url' => route("admin.users.edit", $users->id)
        ],
    ];
    $show = [
//        'show' => [
//            'url' => route("admin.users.show", $users->id)
//        ],
    ];
    $delete = [
        'delete' => [
            'url' => route("admin.users.destroy", $users->id),
            'class' => 'btn-delete',
            'swal_desc' => ' ',
            'swal_confirm' => trans('admin_lang.delete_user_swal_confirm'),
            'swal_cancel' => trans('admin_lang.delete_user_swal_cancel'),
            'swal_title' => trans('admin_lang.delete_user_swal_title')
        ]
    ];
?>

@if(auth()->user()->id == $users->id)
    @php($delete = [])
@else
    @if (
        (!auth()->user()->hasRole('admin') && $users->hasRole('admin')))
    )
        @php($delete = [])
        @php ($edit = [])
    @endif
@endif

@if(!auth()->user()->hasRole('admin'))
    @php($show = [])
@endif


@include('components.dropdown',['links' => array_merge($edit, $delete, $show)])
