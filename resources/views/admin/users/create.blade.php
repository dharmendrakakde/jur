@extends('layouts.admin_main')

@section('title', trans('admin_lang.create_new_user'))

@section('content')
    <!-- begin:: Subheader -->
    @include('components.breadcrumb',[
        'home_url' => route('admin.dashboard.index'),
        'links' => [
            [
                'url' => route('admin.users.index'),
                'title' => trans('admin_lang.users_management'),
            ]
        ],
        'current_page_title' => trans('admin_lang.user_create')
    ])
    <!-- end:: Subheader -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    @include('components.header',['title' => trans('admin_lang.create_new_user'), 'icon' => 'fa fa-user-friends'])
                    <!--begin::Form-->
                    <form class="kt-form" action="{{ route('admin.users.store') }}" method="POST">
                        <div class="kt-portlet__body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="first_name" class="required">@lang('admin_lang.first_name')</label>
                                        <input id="first_name" type="text" name="first_name" placeholder="{{ trans('admin_lang.first_name_placeholder') }}" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="last_name" class="required">@lang('admin_lang.last_name')</label>
                                        <input id="last_name" type="text" name="last_name" placeholder="{{ trans('admin_lang.last_name_placeholder') }}" required class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email" class="required">@lang('admin_lang.email')</label>
                                        <input id="email" type="email" name="email" placeholder="{{ trans('admin_lang.email_placeholder') }}" required class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="role" class="required">@lang('admin_lang.select_role')</label>
                                        <select id="role" name="role" class="form-control">
                                            <option value="">@lang('admin_lang.select_role')</option>
                                            @foreach($roles as $key => $role)
                                                <option value="{{ $key }}">{{ $role }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                @include('components.button',['type' => 'submit'])
                                @include('components.button',['type' => 'cancel', 'url' => route('admin.users.index')])
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
@endsection

