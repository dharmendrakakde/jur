@extends('layouts.admin_main')

@section('title')
    @lang('admin_lang.users_management')
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/css/dataTables.bootstrap4.min.css') }}">
@endpush

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        @include('components.header',['title' => trans('admin_lang.users_management'), 'icon' => 'fa fa-user-friends'])
        <div class="kt-portlet__body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="users-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('admin_lang.avatar')</th>
                        <th>@lang('admin_lang.name')</th>
                        <th>@lang('admin_lang.roles')</th>
                        <th>@lang('admin_lang.email')</th>
                        <th>@lang('admin_lang.last_login')</th>
                        <th>@lang('admin_lang.created')</th>
                        <th>@lang('admin_lang.status')</th>
                        <th>@lang('admin_lang.actions')</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/template" id="header-buttons">
    @include('components.button',['type' => 'create', 'label' => trans('admin_lang.create_new_user'), 'url' => route('admin.users.create')])
</script>

<script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        dataTable(jQuery('#users-table'), "{{ route('admin.users.data') }}", [
                { data: 'index', defaultContent: '', orderable: false, searchable: false },
                { data: 'avatar', name: 'avatar', searchable: false, orderable: false },
                { data: 'name', name: 'name', searchable: true, orderable: true },
                { data: 'roles', name: 'roles', searchable: false, orderable: true  },
                { data: 'email', name: 'email', searchable: true, orderable: true },
                { data: 'last_login_time', name: 'last_login', searchable: false, orderable: true  },
                { data: 'created', name: 'created_at', searchable: false, orderable: true  },
                { data: 'status', name: 'status', searchable: false, orderable: true },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
            "{{ app()->getLocale() }}",
            {!! json_encode($filters) !!},
            jQuery('#header-buttons').html(),
            undefined,
            [[6, 'desc']]
        );
    });

    $(document).on('click','.getDropdown',function(){
        userId = $(this).attr('data-id');
        $('.hide').removeClass('hide');
        $('.setDropdown').remove();
        $(this).addClass('hide');
        htmlDropdown = '<div class="setDropdown"><select name="status" class="form-control setStatus" data-id="'+userId+'">' +
            '<option value="">Select</option>' +
            '<option value="{{ \App\Models\User::ACTIVE }}">{{ trans('admin_lang.status_active') }}</option>' +
            '<option value="{{ \App\Models\User::IN_ACTIVE }}">{{ trans('admin_lang.status_inactive') }}</option>'+
            '<option value="{{ \App\Models\User::PENDING }}">{{ trans('admin_lang.status_pending') }}</option>'+
            '</select></div>';
        //$(this).parent().append(htmlDropdown);
        $(this).html(htmlDropdown);
    });

    $(document).on('change', '.setStatus', function (e) {
        var statusType = $(this).val();
        userId = $(this).attr('data-id');
        if(statusType === "{{ \App\Models\User::ACTIVE }}"){
            htmlBadge = '<span class="kt-badge kt-badge--inline status-active">{{ trans('admin_lang.status_active') }}</span>';
            changeStatus(statusType, userId);
            $(this).closest('.getDropdown').html(htmlBadge).removeClass('hide');
            //$(this).parent().remove();
        } else if(statusType === "{{ \App\Models\User::PENDING }}"){
            htmlBadge = '<span class="kt-badge kt-badge--inline status-pending">{{ trans('admin_lang.status_pending') }}</span>';
            changeStatus(statusType, userId);
            $(this).closest('.getDropdown').html(htmlBadge).removeClass('hide');
            //$(this).parent().remove();
        } else {
            htmlBadge = '<span class="kt-badge kt-badge--inline status-inactive">{{ trans('admin_lang.status_inactive') }}</span>';
            var parent = $(this).closest('.getDropdown');
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                html: "You want to change this!",
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                confirmButtonText: 'Yes, change it!',
                cancelButtonText: 'Cancel',
                showLoaderOnConfirm: true
            }).then(function(confirm) {
                if (confirm.value) {
                    changeStatus(statusType, userId);
                    parent.html(htmlBadge).removeClass('hide');
                    //parent.remove();
                }
            });
        }
        $('.hide').removeClass('hide');
    });

    function changeStatus(status, userId){
        $.ajax({
            method: 'post',
            url: '{{ route('admin.users.changeStatus') }}',
            dataType: 'json',
            async: false,
            data: {
                'userId': userId,
                'status': status,
                '_token': '{{ csrf_token() }}'
            }, success: function (data) {
                if (data.status == true) {
                    successToast("Status updated successfully");
                    $('#datatable').DataTable().draw();
                } else{
                    errorToast("Oops something went wrong!");
                }
            },
            error: function () {
                errorToast("Oops something went wrong!");
            }
        });
    }
</script>
@endsection
