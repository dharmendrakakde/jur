@extends('layouts.email_main')

@section('content')
    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
        <tbody>
        <tr>
            <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                <div style="font-family:Open Sans, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                    <p>
                        <strong>
                            <span style="font-size: 16px; font-family: helvetica, arial, sans-serif;">
                                Hi {{ $user->name }},
                            </span>
                        </strong>
                    </p>
                    {{--<p>--}}
                    {{--<span style="font-size: 14px; font-family: helvetica, arial, sans-serif; color: #74788d;">--}}
                    {{--                            <a href="{{ route('admin.vendors.setPassword',['code' => encrypt($user->verification_code)]) }}">Click here to set your password</a>--}}
                    {{--<!-- <a href="{{ url('vendor/set-password/'.encrypt($user->verification_code)) }}">Click here to set your password</a> -->--}}
                    {{--</span>--}}
                    {{--</p>--}}
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" vertical-align="middle" style="font-size:0px;padding:21px 20px 20px 20px;word-break:break-word;">
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:auto;line-height:100%;">
                    <tbody>
                    <tr>
                        <td align="center" bgcolor="#004aad" role="presentation" style="border:0px solid #000;border-radius:4px;cursor:auto;mso-padding-alt:10px 28px 10px 28px;background:#004aad;" valign="middle">
                            <a href="{{ $setPwdUrl }}" style="display:inline-block;background:#004aad;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:14px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:10px 28px 10px 28px;mso-padding-alt:0px;border-radius:4px;" target="_blank"> Click here to set your password </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection