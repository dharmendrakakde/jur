@extends('layouts.login_main')

@section('title', trans('admin_lang.login'))

@section('css')
    <style>
        .invalid-feedback{
            display:block !important;
        }
    </style>
@endsection
@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-color: white">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            {{--<img src="{{ asset('assets/media/logos/krystal_logo1.png') }}">--}}
                            <h3>{{ config('app.name', 'Laravel') }}</h3>
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">@lang('admin_lang.register')</h3>
                        </div>
                        <form class="kt-form" id="loginForm" action="{{ route('register') }}" method="post">
                            <input type="hidden" name="timezone" id="timezone">
                            {{ csrf_field() }}
                            @if(Session::has('success'))
                                <div class="alert alert-success fade show" role="alert">
                                    <div class="alert-text"><strong>Success!</strong> {{ Session::get('success') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text"><strong>Error!</strong> {{ Session::get('error') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <input id="first_name" type="text" class="form-control" name="first_name" placeholder="{{ trans('admin_lang.first_name') }}" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                                @error('first_name')
                                <span class="invalid-feedback ml-4" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="last_name" type="text" class="form-control" name="last_name" placeholder="{{ trans('admin_lang.last_name') }}" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                                @error('last_name')
                                <span class="invalid-feedback ml-4" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="email" type="email" class="form-control" name="email" placeholder="{{ trans('admin_lang.email') }}" value="{{ old('email') }}" required autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback ml-4" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit" id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">@lang('admin_lang.signUp')</button>
                                <p class="text-black-50 mt-5">@lang('admin_lang.already_have_account')<a href="{{ route('login') }}" class="kt-link kt-login__link">@lang('admin_lang.login')</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#timezone').val(new Date().getTimezoneOffset());
        });
    </script>
@endsection
