@extends('layouts.login_main')

@section('title', trans('admin_lang.login'))

@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-color: white">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            {{--<img src="{{ asset('assets/media/logos/krystal_logo1.png') }}">--}}
                            <h3>{{ config('app.name', 'Laravel') }}</h3>
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">@lang('admin_lang.signInToAdmin')</h3>
                        </div>
                        <form class="kt-form" id="loginForm" action="{{ url('login') }}" method="post">
                            <input type="hidden" name="timezone" id="timezone">
                            {{ csrf_field() }}
                            @if(Session::has('success'))
                                <div class="alert alert-success fade show" role="alert">
                                    <div class="alert-text"><strong>Success!</strong> {{ Session::get('success') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text"><strong>Error!</strong> {{ Session::get('error') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            @if($errors->has('email'))
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text"><strong>Error!</strong> {{ $errors->first('email') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" placeholder="Password" name="password">
                            </div>
                            <div class="row kt-login__extra">
                                <div class="col kt-align-right">
                                    <a href="{{ route('password.request') }}" class="kt-link kt-login__link">@lang('admin_lang.forgetPassword')</a>
                                </div>
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit" id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">@lang('admin_lang.signIn')</button>
                                <p class="text-black-50 mt-5">@lang('admin_lang.textDoNotHaveAC')<a href="{{ route('register') }}" class="kt-link kt-login__link">@lang('admin_lang.linkRegisterHere')</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#timezone').val(new Date().getTimezoneOffset());
        });
    </script>
@endsection
