@extends('layouts.login_main')

@section('title', trans('admin_lang.title_forgetPassword'))

@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-color: white">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            {{--<img src="{{ asset('assets/media/logos/krystal_logo1.png') }}">--}}
                            <h3>{{ config('app.name', 'Laravel') }}</h3>
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">@lang('admin_lang.reset_password')</h3>
                        </div>
                        <form class="kt-form" id="loginForm" action="{{ route('password.email') }}" method="post">
                            <input type="hidden" name="timezone" id="timezone">
                            {{ csrf_field() }}
                            @if (session('status'))
                                <div class="alert alert-success fade show" role="alert">
                                    <div class="alert-text"><strong>Success!</strong> {{ session('status') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif


                            @if($errors->has('email'))
                                <div class="alert alert-danger fade show" role="alert">
                                    <div class="alert-text"><strong>Error!</strong> {{ $errors->first('email') }}</div>
                                    <div class="alert-close">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="la la-close"></i></span>
                                        </button>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}">
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit" id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">@lang('admin_lang.send_password_link')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#timezone').val(new Date().getTimezoneOffset());
        });
    </script>
@endsection