@extends('layouts.login_main')
@section('title', trans('admin_lang.login'))

@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-color: white">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <h3>{{ config('app.name', 'Laravel') }}</h3>
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">{{ __('Verify Your Email Address') }}</h3>
                        </div>

                        <div class="card-body kt-form">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif

                            {{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }},
                            <form class="d-inline" method="POST" action="{{ route('verification.send') }}">
                                @csrf
                                <button type="submit"
                                        class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>
                            </form>
                            <div class="kt-login__actions">
                                <form action="{{ route('logout') }}" method="post">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-pill kt-login__btn-primary" >@lang('admin_lang.signOut')</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection