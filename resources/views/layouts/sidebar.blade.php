<!-- begin:: Aside Menu -->
<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
         data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">
            <li class="kt-menu__item " aria-haspopup="true">
                <a href="{{ route('admin.dashboard.index') }}" class="kt-menu__link {{ Request::is( 'admin/dashboard')  ? 'active' : '' }}">
                    <span class="kt-menu__link-icon">
                        <i class="fa fa-tachometer-alt"></i>
                    </span>
                    <span class="kt-menu__link-text">
                        @lang('admin_lang.module_dashboard')
                    </span>
                </a>
            </li>
            <li class="kt-menu__item " aria-haspopup="true">
                <a href="{{ route('admin.profile') }}" class="kt-menu__link {{ Request::is( 'admin/profile')  ? 'active' : '' }}">
                    <span class="kt-menu__link-icon">
                        <i class="fas fa-address-card"></i>
                    </span>
                    <span class="kt-menu__link-text">
                        @lang('admin_lang.my_profile')
                    </span>
                </a>
            </li>

            @if(hasPermissionWithActiveRole('read-users', auth()->user()))
                <li class="kt-menu__item " aria-haspopup="true">
                    <a href="{{ route('admin.users.index') }}" class="kt-menu__link {{ Request::is( 'admin/users*')  ? 'active' : '' }}">
                        <span class="kt-menu__link-icon user_icon_aside">
                            <i class="fa fa-user-friends"></i>
                        </span>
                        <span class="kt-menu__link-text">@lang('admin_lang.module_users')</span>
                    </a>
                </li>
            @endif

            @if(hasPermissionWithActiveRole('read-roles', auth()->user()))
                <li class="kt-menu__item " aria-haspopup="true">
                    <a href="{{ route('admin.roles.index')  }}" class="kt-menu__link {{ Request::is( 'admin/roles*')  ? 'active' : '' }}">
                        <span class="kt-menu__link-icon user_icon_aside">
                            <i class="fa fa-user-tie"></i>
                        </span>
                        <span class="kt-menu__link-text">@lang('admin_lang.module_roles')</span>
                    </a>
                </li>
            @endif
            <li class="kt-menu__item " aria-haspopup="true">
                <a href="{{ route('admin.work-experiences.index') }}" class="kt-menu__link {{ Request::is( 'admin/work-experiences*')  ? 'active' : '' }}">
                        <span class="kt-menu__link-icon user_icon_aside">
                            <i class="fas fa-hotel"></i>
                        </span>
                    <span class="kt-menu__link-text">@lang('admin_lang.work_experience')</span>
                </a>
            </li>
            <li class="kt-menu__item " aria-haspopup="true">
                <a href="{{ route('admin.organization.index') }}" class="kt-menu__link {{ Request::is( 'admin/organization*')  ? 'active' : '' }}">
                        <span class="kt-menu__link-icon user_icon_aside">
                            <i class="far fa-building"></i>
                        </span>
                    <span class="kt-menu__link-text">@lang('admin_lang.organization')</span>
                </a>
            </li>

        </ul>
    </div>
</div>
<!-- end:: Aside Menu -->
