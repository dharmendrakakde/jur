<!DOCTYPE html>
@if(app()->getLocale() == 'ar')
    <html direction="rtl" dir="rtl" style="direction: rtl">
@else
    <html>
@endif
<!-- <html direction="rtl" dir="rtl" style="direction: rtl" > -->
<!-- begin::Head -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8"/>
    <title>@yield('title') | @lang('admin_lang.admin')</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->
    <!--begin::Page Vendors Styles(used by this page) -->
{{--    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />--}}
    <!--end::Page Vendors Styles -->
    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    @if(app()->getLocale() == 'ar')
        <link title ='en' href="{{ asset('assets/css/style.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />
        <link title ='en' href="{{ asset('assets/css/custom.style.rtl.css') }}" rel="stylesheet" type="text/css" />
    @else
        <link title ='en' href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    @endif
    <!--end::Global Theme Styles -->
    <!--begin::Layout Skins(used by all pages) -->

    <link href="{{ asset('assets/css/skins/header/base/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/skins/brand/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.rawgit.com/filamentgroup/fixed-sticky/master/fixedsticky.css" rel="stylesheet" type="text/css" />
    <!-- <link href="{{ asset('assets/css/skins/aside/dark.css') }}" rel="stylesheet" type="text/css" /> -->
    <link href="{{asset('css/loader.css')}}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/fav-icon-2.png') }}" />
    <link href="{{ url('css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #kt_subheader + .kt-container,
        #kt_subheader + .order_detail_section,
        #kt_subheader + .kt-portlet {
            margin-top: 50px;
        }

        table.table-bordered.dataTable tbody th,
        .dataTables_wrapper .dataTable thead tr th {
            white-space: nowrap;
        }

        table [title] {
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            width: 6rem;
            display: block;
        }
    </style>
    @yield('head-js')
    @yield('css')
    @stack('styles')
</head>
<!-- end::Head -->
<!-- begin::Body -->
<body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading"  >
    <div class="loading" style="display: none;">Loading&#8230;</div>
    <!-- begin:: Page -->
    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed " >
        <div class="kt-header-mobile__logo">
            <a href="javascript:void(0);"> <img alt="Logo" src="{{ asset('assets/media/logos/logo-light.png') }}"/> </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <!-- begin:: Aside -->
            <!-- Uncomment this to display the close button of the panel -->
            <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
            <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
                <!-- begin:: Aside -->
                <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                    <div class="kt-aside__brand-logo"style="padding: 0 25px 0 0">
                        {{--<a href="javascript:void(0);"> <img alt="Logo" src="{{ asset('assets/media/logos/logo-light.png') }}"/> </a>--}}
                    </div>
                    <div class="kt-aside__brand-tools">
                        <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler" style="color: #83162c;">
                            <span>
                                <i class="fa fa-2x fa-angle-double-left"></i>
                            </span>
                            <span>
                                <i class="fa fa-2x fa-angle-double-right"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <!-- end:: Aside -->
                @include('layouts.sidebar')
            </div>
            <!-- end:: Aside -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper" style="padding-top: 50px;">
                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed" >
                    @include('layouts.header')
                </div>
                <!-- end:: Header -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                    <!-- begin:: Content -->
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        @yield('content')
                    </div>
                    <!-- end:: Content -->
                </div>
                <!-- begin:: Footer -->
                <div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                    <div class="kt-container  kt-container--fluid ">
                        <div class="kt-footer__copyright">
                            <?php echo date(
                                'Y'
                            ); ?>&nbsp;&copy;&nbsp;@lang('admin_lang.app_name')
                        </div>
                    </div>
                </div>
                <!-- end:: Footer -->
            </div>
        </div>
    </div>
    <!-- end:: Page -->
    <!-- begin::Quick Panel -->
    <div id="kt_quick_panel" class="kt-quick-panel">
        <a href="#" class="kt-quick-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></a>
        <div class="kt-quick-panel__nav">
            <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
                <li class="nav-item active">
                    <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_logs" role="tab">Audit Logs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- end::Quick Panel -->
    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var en_css_url = "{{ asset('assets/css/style.bundle.rtl.css') }}";
        var en_customCss_url = "{{ asset('assets/css/custom.style.rtl.css') }}";

        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
    <!-- end::Global Config -->
    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

    <script src="https://cdn.rawgit.com/filamentgroup/fixed-sticky/master/fixedsticky.js"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/pages/components/extended/toastr.js') }}" type="text/javascript"></script>

    <script>
          $( '.map_block' ).fixedsticky();
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        /** loading overlay start */
        function showLoading() {
            $('.loading').css('display', 'block');
        }

        function hideLoading() {
            $('.loading').css('display', 'none');
        }

        /** loading overlay end */

        $('textarea').each(function () {
                $(this).val($(this).val().trim());
            }
        );

        @if(session('success'))
            toastr.success("{!! session('success') !!}", "{!! trans('admin_lang.success') !!}");
        @endif
        @if(session('warning'))
            toastr.warning("{!! session('warning') !!}", "{!! trans('admin_lang.warning') !!}");
        @endif
        @if(session('error'))
            toastr.error("{!! session('error') !!}", "{!! trans('admin_lang.error') !!}");
        @endif
        function successToast(msg) {
            toastr.success(msg, "{!! trans('admin_lang.success') !!}");
        }
        function errorToast(msg) {
            toastr.error(msg, "{!! trans('admin_lang.error') !!}");
        }
        function warningToast(msg) {
            toastr.warning(msg, "{!! trans('admin_lang.error') !!}");
        }
    </script>
    <!--end::Page Scripts -->
    <script type="text/javascript">
        $(document).ready(function(){
            var csrfToken = '{!! csrf_token() !!}';
        });

        $( document ).ready(function() {
            $('#timezone').val(new Date().getTimezoneOffset());
        });
    </script>
    @yield('js')
    @stack('scripts')
</body>
<!-- end::Body -->
</html>
