<!-- begin:: Header Menu -->
{{--Uncomment this to display the close button of the panel--}}
{{--<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>--}}

<div class=" container-fluid  d-flex align-items-stretch justify-content-between">
    <!-- end:: Header Menu -->
    <div class="header-menu-wrapper header-menu-wrapper-left">
        <div class="store_name">
                {{--<a href="{{ '/' }}">--}}
                    {{--<span class="svg-icon svg-icon-2x"><!--begin::Svg Icon | path:/home/keenthemes/www/metronic/themes/metronic/theme/html/demo1/dist/../src/media/svg/icons/Home/Home.svg-->--}}
                        {{--<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">--}}
                            {{--<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
                                {{--<rect x="0" y="0" width="24" height="24"/>--}}
                                {{--<path d="M13.5,21 L13.5,18 C13.5,17.4477153 13.0522847,17 12.5,17 L11.5,17 C10.9477153,17 10.5,17.4477153 10.5,18 L10.5,21 L5,21 L5,4 C5,2.8954305 5.8954305,2 7,2 L17,2 C18.1045695,2 19,2.8954305 19,4 L19,21 L13.5,21 Z M9,4 C8.44771525,4 8,4.44771525 8,5 L8,6 C8,6.55228475 8.44771525,7 9,7 L10,7 C10.5522847,7 11,6.55228475 11,6 L11,5 C11,4.44771525 10.5522847,4 10,4 L9,4 Z M14,4 C13.4477153,4 13,4.44771525 13,5 L13,6 C13,6.55228475 13.4477153,7 14,7 L15,7 C15.5522847,7 16,6.55228475 16,6 L16,5 C16,4.44771525 15.5522847,4 15,4 L14,4 Z M9,8 C8.44771525,8 8,8.44771525 8,9 L8,10 C8,10.5522847 8.44771525,11 9,11 L10,11 C10.5522847,11 11,10.5522847 11,10 L11,9 C11,8.44771525 10.5522847,8 10,8 L9,8 Z M9,12 C8.44771525,12 8,12.4477153 8,13 L8,14 C8,14.5522847 8.44771525,15 9,15 L10,15 C10.5522847,15 11,14.5522847 11,14 L11,13 C11,12.4477153 10.5522847,12 10,12 L9,12 Z M14,12 C13.4477153,12 13,12.4477153 13,13 L13,14 C13,14.5522847 13.4477153,15 14,15 L15,15 C15.5522847,15 16,14.5522847 16,14 L16,13 C16,12.4477153 15.5522847,12 15,12 L14,12 Z" fill="#000000"/>--}}
                                {{--<rect fill="#FFFFFF" x="13" y="8" width="3" height="3" rx="1"/>--}}
                                {{--<path d="M4,21 L20,21 C20.5522847,21 21,21.4477153 21,22 L21,22.4 C21,22.7313708 20.7313708,23 20.4,23 L3.6,23 C3.26862915,23 3,22.7313708 3,22.4 L3,22 C3,21.4477153 3.44771525,21 4,21 Z" fill="#000000" opacity="0.3"/>--}}
                            {{--</g>--}}
                        {{--</svg>--}}
                    {{--</span>--}}
                    {{--<span class="st_label">{{ trans('admin_lang.company') }}:</span>--}}
                    {{--<span class="st_name">{{ 'Test Company' }}</span>--}}
                    {{--@lang('admin_lang.change_company')--}}
                {{--</a>--}}
            </div>
    </div>
    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user show">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px" aria-expanded="true">
                <div class="kt-header__topbar-user">
                    <span class="kt-header__topbar-welcome kt-hidden-mobile">@lang('admin_lang.hi'),</span>
                    <span class="kt-header__topbar-username kt-hidden-mobile">{{ auth()->user()->name }}</span>
                    @php($default_url = asset('/images/user-hero.png'))
                    <div class="at_profile">
                        <img src="{{ $default_url }}" onerror="this.src='{{ $default_url }}'" alt="{{ auth()->check() ? auth()->user()->name : 'Guest' }}">
                    </div>
                    <i class="kt-menu__ver-arrow la la-angle-down"></i>
                    {{--<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">S</span>--}}
                </div>
            </div>

            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl" style="width:25%;">
                <!--begin: Head -->
                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url('{{ asset('assets/media/misc/bg-1.jpg') }}')">
                    <div class="kt-user-card__avatar">
                        @if(auth()->user()->avatar != null)
                            <img alt="avatar" src="{{ asset(auth()->user()->avatar) }}">
                        @else
                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ ucwords(auth()->user()->name[0]) }}</span>
                        @endif
                    </div>
                    <div class="kt-user-card__name">
                        {{ auth()->user()->name }}
                    </div>
                </div>
                <!--end: Head -->

                <!--begin: Navigation -->
                <div class="kt-notification">
                    <a href="{{ route('admin.profile') }}" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                @lang('admin_lang.my_profile')
                            </div>
                            <div class="kt-notification__item-time">
                                @lang('admin_lang.account_settings')
                            </div>
                        </div>
                    </a>
                    <div class="kt-notification__custom kt-space-between" style="padding: 15px;">
                        <form action="{{ route('logout') }}" method="post" style="margin: 0;">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-label btn-label-brand btn-sm btn-bold" style="width: 150%">@lang('admin_lang.signOut')</button>
                        </form>
                    </div>
                </div>
                <!--end: Navigation -->
            </div>
        </div>
        <!--end: User Bar -->
    </div>
    <!-- end:: Header Topbar -->
</div>
