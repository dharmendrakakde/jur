<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title></title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a { padding:0; }
        .ReadMsgBody { width:100%; }
        .ExternalClass { width:100%; }
        .ExternalClass * { line-height:100%; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
        a {text-decoration: none; }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media only screen and (max-width:480px) {
            @-ms-viewport { width:320px; }
            @viewport { width:320px; }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>

    <style type="text/css">
      .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet" type="text/css" />
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Cabin:400,700);
    </style>
    <!--<![endif]-->

    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 { width:100% !important; max-width: 100%; }
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width:480px) {
            table.full-width-mobile { width: 100% !important; }
            td.full-width-mobile { width: auto !important; }
        }
    </style>
    <style type="text/css">
        .hide_on_mobile { display: none !important;}
        @media only screen and (min-width: 480px) { .hide_on_mobile { display: block !important;} }
        .hide_section_on_mobile { display: none !important;}
        @media only screen and (min-width: 480px) { .hide_section_on_mobile { display: table !important;} }
        .hide_on_desktop { display: block !important;} 
        @media only screen and (min-width: 480px) { .hide_on_desktop { display: none !important;} }
        .hide_section_on_desktop { display: table !important;} 
        @media only screen and (min-width: 480px) { .hide_section_on_desktop { display: none !important;} }
        [owa] .mj-column-per-100 {
            width: 100% !important;
        }
        [owa] .mj-column-per-50 {
            width: 50% !important;
        }
        [owa] .mj-column-per-33 {
            width: 33.333333333333336% !important;
        }
        p {
            margin: 0px;
        }
        @media only print and (min-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
            }

            .mj-column-per-40 {
                width: 40% !important;
            }

            .mj-column-per-60 {
                width: 60% !important;
            }

            .mj-column-per-50 {
                width: 50% !important;
            }

            mj-column-per-33 {
                width: 33.333333333333336% !important;
            }
        }
    </style>
</head>
<body style="background-color:#f3f3f3;">
    <div style="background-color:#f3f3f3;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f3f3f3;background-color:#f3f3f3;width:100%;">
            <tbody>
                <tr>
                    <td>
                        <!--[if mso | IE]>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:655px;" width="655">
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                    <![endif]-->
                                    <div style="margin:0px auto;max-width:655px;">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="direction:ltr;font-size:0px;padding:9px 0px 0px 0px;text-align:center;vertical-align:top;">
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="" style="vertical-align:top;width:655px;">
                                                                <![endif]-->
                                                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center" class="hide_on_mobile" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="width:655px;">
                                                                                                    <img height="auto" src="{{ asset('/images/1591861422.jpg') }}" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="655" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <!--[if mso | IE]>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <!--[if mso | IE]>
                                </td>
                            </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </tbody>
      </table>

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f3f3f3;background-color:#f3f3f3;width:100%;">
            <tbody>
                <tr>
                    <td>
                        <!--[if mso | IE]>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:655px;" width="655">
                            <tr>
                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                        <![endif]-->
                                    <div style="Margin:0px auto;max-width:655px;">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="direction:ltr;font-size:0px;padding:0px 0px 3px 0px;text-align:center;vertical-align:top;">
                                                        <!--[if mso | IE]>
                                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="" style="vertical-align:top;width:655px;">
                                                        <![endif]-->
                                                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="background-color:#fffff;border-radius:4px;vertical-align:top;padding:0px 45px 0px 45px;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left" style="background:#FFFFFF;font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">
                                                                                                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                                                                                                            <p style="text-align: center;">
                                                                                                                <strong><span style="font-size: 24px;">@yield('email_subject')</span></strong>
                                                                                                            </p>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="background:#FFFFFF;font-size:0px;padding:15px 30px 15px 30px;word-break:break-word;">
                                                                                                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                                                                                                        @yield('content')
{{--<div><span style="font-size: 14px;"><span style="color: #585a5c;">Hi</span> <strong>Luffy</strong></span></div>--}}
{{--<div>&#xA0;</div>--}}
{{--<div><span style="font-size: 14px;">To finish setting up your account, we just need to make sure that this email address is yours.</span></div>--}}
{{--<div>&#xA0;</div>--}}
{{--<div><span style="font-size: 14px;">We will help you promote your products through our website. For further help get in touch through support Chat.</span></div>--}}
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
           {{----}}
            {{--<tr>--}}
              {{--<td align="left" style="background:#FFFFFF;font-size:0px;padding:15px 30px 15px 30px;word-break:break-word;">--}}
                {{----}}
      {{--<div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">--}}
      		{{--<a href="http://#" target="_blank">--}}
        {{--<p><span style="text-decoration: underline; color: #004aad;"><strong><span style="font-family: helvetica, arial, sans-serif;"><span style="font-size: 14px;">Verify Email</span></span></strong></span></p>--}}
    	{{--</a>--}}
      {{--</div>--}}
    {{----}}
              {{--</td>--}}
            {{--</tr>--}}
          {{----}}
            {{--<tr>--}}
              {{--<td align="center" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">--}}
                {{----}}
      {{--<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">--}}
        {{--<tbody>--}}
          {{--<tr>--}}
            {{--<td style="width:565px;">--}}
              {{----}}
        {{--<a href="http://#" target="_blank">--}}
          {{----}}
      {{--<img height="auto" src="./images/1591860438.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="565">--}}
    {{----}}
        {{--</a>--}}
      {{----}}
            {{--</td>--}}
          {{--</tr>--}}
        {{--</tbody>--}}
      {{--</table>--}}
    {{----}}
              {{--</td>--}}
            {{--</tr>--}}
          {{----}}
            {{--<tr>--}}
              {{--<td align="left" style="background:#FFFFFF;font-size:0px;padding:15px 30px 15px 30px;word-break:break-word;">--}}
                {{----}}
      {{--<div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">--}}
        {{--<p><span style="color: #585a5c; font-size: 14px;">Best regards,</span></p>--}}
{{--<p><strong><span style="font-size: 14px;">Bycost Team</span></strong></p>--}}
      {{--</div>--}}
    {{----}}
              {{--</td>--}}
            {{--</tr> --}}

                                                                                                <tr>
                                                                                                    <td style="background:#FFFFFF;font-size:0px;padding:10px 25px;padding-top:5px;padding-right:30px;padding-bottom:5px;padding-left:30px;word-break:break-word;">
                                                                                                        <p style="border-top:solid 1px #E1E1E1;font-size:1;margin:0px auto;width:100%;"></p>
                                                                                                        <!--[if mso | IE]>
                                                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #E1E1E1;font-size:1;margin:0px auto;width:505px;" role="presentation" width="505px">
                                                                                                            <tr>
                                                                                                                <td style="height:0;line-height:0;">&nbsp;</td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <![endif]-->
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="background:#FFFFFF;font-size:0px;padding:15px 30px 15px 30px;word-break:break-word;">
                                                                                                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.8;text-align:left;color:#000000;">
                                                                                                            <p><span style="font-size: 13px;"><span style="color: #585a5c;">Office:</span><a href="tel:1236549870" style="color: #004aad;"> <strong>(123)-654-9870</strong></a></span></p>
                                                                                                            <p><span style="font-size: 13px;"><span style="color: #585a5c;">Email</span><strong><span style="color: #585a5c;">:</span><a href="mailTo:xyzabc@mail.com"> <span style="color: #004aad;">xyzabc@mail.com</span></a></strong></span></p>
                                                                                                            <p><span style="font-size: 13px;"><span style="color: #585a5c;">Website:</span><a href="http://codmgmt.com" target="_blank"><span style="color: #004aad;"><strong> codmgmt </strong></span></a></span></p>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center" style="font-size:0px;padding:20px 10px 0px 10px;word-break:break-word;">
                                                                                                        <!--[if mso | IE]>
                                                                                                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation">
                                                                                                            <tr>
                                                                                                                <td>
                                                                                                                    <![endif]-->
                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding:4px;">
                                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:25px;">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:0;height:25px;vertical-align:middle;width:25px;">
                                                                                                                                                    <a href="https://www.facebook.com/PROFILE" target="_blank">
                                                                                                                                                        <img height="25" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/facebook.png" style="border-radius:3px;display:block;" width="25" />
                                                                                                                                                    </a>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <!--[if mso | IE]>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <![endif]-->
                                                                                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td style="padding:4px;">
                                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:transparent;border-radius:3px;width:25px;">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td style="font-size:0;height:25px;vertical-align:middle;width:25px;">
                                                                                                                                                    <a href="https://www.twitter.com/PROFILE" target="_blank">
                                                                                                                                                        <img height="25" src="https://s3-eu-west-1.amazonaws.com/ecomail-assets/editor/social-icos/ikony-black/roundedblack/twitter.png" style="border-radius:3px;display:block;" width="25">
                                                                                                                                                    </a>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                    <!--[if mso | IE]>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <![endif]-->
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="left" style="font-size:0px;padding:0px 15px 15px 15px;word-break:break-word;">
                                                                                                        <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                                                                                                            <div>&#xA0;</div>
                                                                                                            <p style="text-align: center;"><span style="font-size: 12px; color: #393939;">{{ date('Y') }} &#xA9; | All Rights Reserved by {{ config('app.name') }}</span></p>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                        <!--[if mso | IE]>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                        <!--[if mso | IE]>
                                </td>
                            </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>