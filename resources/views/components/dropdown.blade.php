@if (!empty($links))
<span style="overflow: visible; position: relative; width: 80px;">
    <div class="dropdown">
        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">
            <i class="flaticon-more-1"></i>
        </a>
        <div class="dropdown-menu {{ $dropdown_class ?? 'dropdown-menu-right' }}">
            <ul class="kt-nav">
                @foreach($links as $type => $link)
                    <li class="kt-nav__item">
                        @php
                            $swal = "";
                            if (!empty($link['swal_title']))
                                $swal .= ('swal-title='.str_replace(' ', '&nbsp;', $link['swal_title']));
                            if (!empty($link['swal_desc']))
                                $swal .= (' swal-description='.str_replace(' ', '&nbsp;', $link['swal_desc']));
                            if (!empty($link['swal_confirm']))
                                $swal .= (' swal-confirm='.str_replace(' ', '&nbsp;', $link['swal_confirm']));
                            if (!empty($link['swal_cancel']))
                                $swal .= (' swal-cancel='.str_replace(' ', '&nbsp;', $link['swal_cancel']));
                        @endphp
                        <a href="{{ $link['url'] ?? 'javascript:void(0)' }}" target="{{ $link['target'] ?? '' }}" class="kt-nav__link {{ $link['class'] ?? '' }}" {{ $swal }}>
                            <i class="kt-nav__link-icon fa fa-1x {{ $link['icon'] ?? config('actions')[$type]['icon'] }}"></i>
                            <span class="kt-nav__link-text">{{ $link['label'] ?? trans(config('actions')[$type]['label']) }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</span>
@else
    <span><i class="fa fa-times"></i></span>
@endif