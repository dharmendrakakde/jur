<div id="{{ $id ?? 'lang-tab'  }}" data-current-locale="{{ app()->getLocale() }}">
    <ul class="nav nav-pills" role="tablist">
        @foreach($languages as $lang)
            <li class="nav-item">
                <a class="nav-link {{ ($lang == app()->getLocale()) ? 'active' : '' }}" data-toggle="tab" href="#tab-lang-{{ $lang }}">{{ $lang }}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($languages as $lang)
            <div class="tab-pane {{ ($lang == app()->getLocale()) ? 'active' : '' }}" id="{{ $id ?? 'tab-lang'  }}-{{$lang}}" data-locale="{{ $lang }}">
                {!! str_replace('%lang%', $lang, $slot) !!}
            </div>
        @endforeach
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            let currentLocale = jQuery('#lang-tab').attr('data-current-locale');

            //select all values if page is edit page
            jQuery('#lang-tab .tab-pane input, #lang-tab .tab-pane textarea, #lang-tab .tab-pane select').each(function(key, element) {
                let locale = jQuery(this).closest('.tab-pane').attr('data-locale');
                let values = jQuery(element).attr('data-value');
                if (typeof values != 'undefined') {
                    //if input element is select2 then reinvoke it
                    if (jQuery(element).hasClass('select2')) {
                        jQuery(element).select2({data: JSON.parse(values)[locale]});
                        jQuery(element).val(JSON.parse(values)[locale]);
                        jQuery(this).select2().trigger('change');
                    } else {
                        jQuery(element).val(JSON.parse(values)[locale]);
                    }
                }
                if (locale == currentLocale || values) {
                    jQuery(element).attr('data-change', false);
                } else {
                    jQuery(element).attr('data-change', true);
                }
            });

            //change all untouched inputs when current locale input is edited
            jQuery(document).on(
                'input', 
                '#lang-tab .tab-pane[data-locale="' + currentLocale + '"] input, #lang-tab .tab-pane[data-locale="' + currentLocale + '"] textarea',
                function() {
                    let name = jQuery(this).attr('name').split("[" + currentLocale + "]")[0];
                    jQuery('#lang-tab [name^="' + name + '["][data-change=true]').val(jQuery(this).val());
            });

            //change summernotes on change
            jQuery(document).on('summernote.change', '#lang-tab .tab-pane[data-locale="' + currentLocale + '"] .summernote', function() {
                let content = jQuery(this).summernote('code');
                let name = jQuery(this).attr('name').split("[" + currentLocale + "]")[0];
                jQuery('#lang-tab [name^="' + name + '["][data-change=true]').summernote('code', content);
            });
            
            //disable autochange when one input is edited
            jQuery(document).on(
                'input', 
                '#lang-tab .tab-pane:not([data-locale="' + currentLocale + '"]) input, #lang-tab .tab-pane:not([data-locale="' + currentLocale + '"]) textarea',
                function() {
                    jQuery(this).attr('data-change', false);
            });

            //disable autochange when one summernote input is edited
            jQuery(document).on('summernote.blur', '#lang-tab .tab-pane:not([data-locale="' + currentLocale + '"]) .summernote', function() {
                jQuery(this).attr('data-change', false);
            });
        });
    </script>
@endpush