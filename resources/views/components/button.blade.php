@switch ($type)
    @case('create')
        <a href="{{ $url ?? 'javascript:void(0)' }}" class="{{ $class ?? 'btn btn-brand btn-elevate btn-icon-sm' }}"><i class="{{ $icon ?? 'fa fa-1x fa-plus' }}"></i> {{ $label }}</a>
        @break
    @case('edit')
        <a href="{{ $url ?? 'javascript:void(0)' }}" class="{{ $class ?? 'btn btn-info btn-sm' }}"><i class="{{ $icon ?? 'fa fa-1x fa-edit' }}"></i> {{ $label ?? trans('admin_lang.edit_btn_label') }}</a>
        @break
    @case('show')
        <a href="{{ $url ?? 'javascript:void(0)' }}" class="{{ $class ?? 'btn btn-success btn-sm' }}"><i class="{{ $icon ?? 'fa fa-1x fa-eye' }}"></i> {{ $label ?? trans('admin_lang.show_btn_label') }}</a>
        @break
    @case('destroy')
        <a href="{{ $url ?? 'javascript:void(0)' }}" class="{{ $class ?? 'btn btn-danger btn-sm btn-delete' }}" swal-description="{{ $swal_desc ?? trans('admin_lang.delete_user_swal_description') }}" swal-confirm="{{ $swal_confirm ?? trans('admin_lang.delete_user_swal_confirm') }}" swal-cancel="{{ $swal_cancel ?? trans('admin_lang.delete_user_swal_cancel') }}" swal-title="{{ $swal_title ?? trans('admin_lang.delete_user_swal_title') }}" ><i class="{{ $icon ?? 'fa fa-1x fa-trash' }}"></i> {{ $label ?? trans('admin_lang.delete_btn_label') }}</a>
        @break
    @case('submit')
        <button type="submit" class="{{ $class ?? 'btn btn-primary ajax-submit' }}">{{ $label ?? trans('admin_lang.submit') }}</button>
        @break
    @case('update')
        <button type="submit" class="{{ $class ?? 'btn btn-primary ajax-submit' }}">{{ $label ?? trans('admin_lang.update') }}</button>
        @break
    @case('cancel')
        <a href="{{ $url ?? 'javascript:void' }}" class="{{ $class ?? 'btn btn-secondary' }}">{{ $label ?? trans('admin_lang.cancel') }}</a>
        @break
    @default
    <a href="{{ $url ?? 'javascript:void' }}" target="{{ $target ?? '' }}" class="{{ $class ?? 'btn btn-primary' }}"><i class="{{ $icon ?? '' }}"></i>{{ $label }}</a>
@endswitch
