<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-subheader__main">
            <div class="kt-subheader__breadcrumbs">
                <a href="{{ $home_url }}" class="kt-subheader__breadcrumbs-home">
                    <i class="flaticon2-shelter"></i>
                </a>
                @if(isset($links))
                    @foreach($links as $link)
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="{{ $link['url'] ?? 'javascript:void(0)' }}" class="kt-subheader__breadcrumbs-link">
                            {{ $link['title'] ?? '' }}
                        </a>
                    @endforeach
                @endif
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">
                    {{ $current_page_title ?? 'No Title' }}
                </span>
            </div>
        </div>
    </div>
</div>
<!-- end:: Subheader -->
